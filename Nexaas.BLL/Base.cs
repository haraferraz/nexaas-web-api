﻿using Nexaas.Model.UnitOfWork;
using AutoMapper;

namespace Nexaas.BLL
{
    public class Base
    {
        protected readonly IMapper _mapper;
        protected readonly IUnitOfWork _unitOfWork;

        public Base(IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }
    }
}