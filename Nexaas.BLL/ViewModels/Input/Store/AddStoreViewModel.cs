﻿using System.ComponentModel.DataAnnotations;

namespace Nexaas.BLL.ViewModels
{
    public class AddStoreViewModel
    {
        [Required(ErrorMessage = "Informe o CNPJ sem pontuação")]
        [MinLength(14, ErrorMessage = "O CNPJ deve ter 14 caracteres (sem pontuação).")]
        [MaxLength(14, ErrorMessage = "O CNPJ deve ter 14 caracteres (sem pontuação).")]
        public string CNPJ { get; set; }

        [Required(ErrorMessage = "Informe nome")]
        [MaxLength(150, ErrorMessage = "O nome deve ter no máximo 150 caracteres.")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Informe o CEP do endereço")]
        [MinLength(8, ErrorMessage = "O CEP deve ter 8 caracteres (sem pontuação).")]
        [MaxLength(8, ErrorMessage = "O CEP deve ter 8 caracteres (sem pontuação).")]
        public string CEP { get; set; }

        [Required(ErrorMessage = "Informe o logradouro (rua) do endereço")]
        [MinLength(8, ErrorMessage = "O logradouro (rua) deve ter no mínimo 8 caracteres.")]
        [MaxLength(150, ErrorMessage = "O logradouro (rua) deve ter no máximo 150 caracteres.")]
        public string Logradouro { get; set; }

        public int? Numero { get; set; }

        [MaxLength(150, ErrorMessage = "O complemento deve ter no máximo 150 caracteres.")]
        public string Complemento { get; set; }

        [Required(ErrorMessage = "Informe o bairro do endereço")]
        [MinLength(8, ErrorMessage = "O bairro deve ter no mínimo 8 caracteres.")]
        [MaxLength(50, ErrorMessage = "O bairro deve ter no máximo 50 caracteres.")]
        public string Bairro { get; set; }

        [Required(ErrorMessage = "Informe o município do endereço")]
        [MinLength(8, ErrorMessage = "O município deve ter no mínimo 8 caracteres.")]
        [MaxLength(150, ErrorMessage = "O município deve ter no máximo 150 caracteres.")]
        public string Municipio { get; set; }

        [Required(ErrorMessage = "Informe o UF (estado) do endereço")]
        [MinLength(2, ErrorMessage = "O UF (estado) deve ter no mínimo 2 caracteres.")]
        [MaxLength(2, ErrorMessage = "O UF (estado) deve ter no máximo 2 caracteres.")]
        public string UF { get; set; }
    }
}