﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Nexaas.BLL.ViewModels
{
    public class RemoveStoreViewModel
    {
        [Required(ErrorMessage = "Informe o ID da loja")]
        public Guid ID { get; set; }

        public bool SoftDelete { get; set; }
    }
}