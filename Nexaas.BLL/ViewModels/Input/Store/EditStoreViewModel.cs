﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Nexaas.BLL.ViewModels
{
    public class EditStoreViewModel : AddStoreViewModel
    {
        [Required(ErrorMessage = "Informe o ID da loja")]
        public Guid ID { get; set; }
    }
}