﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Nexaas.BLL.ViewModels
{
    public class AddStockItemViewModel
    {
        [Required(ErrorMessage = "Informe o ID da loja")]
        public Guid? IDStore { get; set; }

        [Required(ErrorMessage = "Informe o ID do produto")]
        public Guid? IDProduct { get; set; }

        [Required(ErrorMessage = "Informe o preço de venda")]
        public decimal PrecoVenda { get; set; }

        [Required(ErrorMessage = "Informe a quantidade")]
        public int Quantidade { get; set; }
    }
}