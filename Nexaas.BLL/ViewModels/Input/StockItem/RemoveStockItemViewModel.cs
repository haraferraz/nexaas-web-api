﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Nexaas.BLL.ViewModels
{
    public class RemoveStockItemViewModel
    {
        [Required(ErrorMessage = "Informe o ID da loja")]
        public Guid? IDStore { get; set; }

        [Required(ErrorMessage = "Informe o ID do produto")]
        public Guid? IDProduct { get; set; }

        public int Quantidade { get; set; }

        public bool RemoverDoEstoque { get; set; }
    }
}