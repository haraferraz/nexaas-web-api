﻿using System;

namespace Nexaas.BLL.ViewModels
{
    public class GetStockItemViewModel
    {
        public Guid? IDStore { get; set; }
        public Guid? IDProduct { get; set; }
        public string SKU { get; set; }
    }
}