﻿using System;

namespace Nexaas.BLL.ViewModels
{
    public class GetIDViewModel
    {
        public Guid ID { get; set; }
    }
}