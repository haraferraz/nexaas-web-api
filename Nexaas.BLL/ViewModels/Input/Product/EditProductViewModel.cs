﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Nexaas.BLL.ViewModels
{
    public class EditProductViewModel
    {
        [Required(ErrorMessage = "Informe o ID do produto")]
        public Guid ID { get; set; }

        [Required(ErrorMessage = "Informe o nome")]
        [MinLength(8, ErrorMessage = "O nome deve ter no mínimo 8 caracteres.")]
        [MaxLength(150, ErrorMessage = "O nome deve ter no máximo 150 caracteres.")]
        public string Nome { get; set; }

        [Required(ErrorMessage = "Informe o preço de custo")]
        public decimal PrecoCusto { get; set; }
    }
}