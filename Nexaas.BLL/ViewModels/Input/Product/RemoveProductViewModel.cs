﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Nexaas.BLL.ViewModels
{
    public class RemoveProductViewModel
    {
        [Required(ErrorMessage = "Informe o ID do produto")]
        public Guid ID { get; set; }

        public bool SoftDelete { get; set; }
    }
}