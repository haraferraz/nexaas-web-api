﻿using System.ComponentModel.DataAnnotations;

namespace Nexaas.BLL.ViewModels
{
    public class AddProductViewModel
    {
        [Required(ErrorMessage = "Informe o nome")]
        [MinLength(8, ErrorMessage = "O nome deve ter no mínimo 8 caracteres.")]
        [MaxLength(150, ErrorMessage = "O nome deve ter no máximo 150 caracteres.")]
        public string Nome { get; set; }

        [MaxLength(20, ErrorMessage = "O SKU deve ter no máximo 20 caracteres.")]
        public string SKU { get; set; }

        [Required(ErrorMessage = "Informe o preço de custo")]
        public decimal PrecoCusto { get; set; }
    }
}