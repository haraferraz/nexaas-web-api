﻿using System;

namespace Nexaas.BLL.ViewModels
{
    public class StockItemViewModel
    {
        public Guid ID { get; set; }
        public string Nome { get; set; }
        public string SKU { get; set; }
        public string PrecoCusto { get; set; }
        public string PrecoVenda { get; set; }
        public string Quantidade { get; set; }
    }
}