﻿using System;

namespace Nexaas.BLL.ViewModels
{
    public class ProductViewModel
    {
        public Guid ID { get; set; }
        public string Nome { get; set; }
        public string SKU { get; set; }
        public string PrecoCusto { get; set; }
    }
}