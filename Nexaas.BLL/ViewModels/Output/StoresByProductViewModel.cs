﻿using System.Collections.Generic;

namespace Nexaas.BLL.ViewModels
{
    public class StoresByProductViewModel : StockItemViewModel
    {
        public StoreViewModel Store { get; set; }
    }
}