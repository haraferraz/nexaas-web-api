﻿using System.Collections.Generic;

namespace Nexaas.BLL.ViewModels
{
    public class ProductsByStoreViewModel : StoreViewModel
    {
        public ICollection<StockItemViewModel> Products { get; set; }
    }
}