﻿namespace Nexaas.BLL.Classes
{
    public class Mensagens
    {
        public string ParametrosIncorretos { get; set; }
        public string AcessoActionNegado { get; set; }
        public string ApenasHttps { get; set; }
        public string BaseInvalida { get; set; }
        public string ChaveExpirada { get; set; }
        public string ErroGenerico { get; set; }
        public string ErroVerificacaoChave { get; set; }
        public string FalhaAtualizarRegistro { get; set; }
        public string FalhaBuscarRegistro { get; set; }
        public string FalhaDeletarRegistro { get; set; }
        public string FalhaInsercaoRegistro { get; set; }
        public string ListaVazia { get; set; }
        public string MetodoExecutado { get; set; }
        public string NaoEncontrado { get; set; }
        public string NenhumRetorno { get; set; }
        public string ParametroVazio { get; set; }
        public string RegistroInserido { get; set; }
        public string TokenInvalido { get; set; }
    }
}