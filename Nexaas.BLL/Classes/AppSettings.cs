﻿namespace Nexaas.BLL.Classes
{
    public class AppSettings
    {
        public string ChavePrivadaApi { get; set; }
        public string TokenAudience { get; set; }
        public string TokenIssuer { get; set; }
        public bool UsarHTTPS { get; set; }
        public int ValidadeToken { get; set; }
    }
}