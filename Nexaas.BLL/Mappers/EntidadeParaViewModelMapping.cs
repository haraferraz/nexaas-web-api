using AutoMapper;
using Nexaas.Model.Models;
using Nexaas.BLL.ViewModels;

namespace Nexaas.BLL.Mappers
{
    public class EntidadeParaViewModelMapping : Profile
    {
        public EntidadeParaViewModelMapping()
        {
            // Store

            CreateMap<Store, StoreViewModel>()
                .ForMember(a => a.ID, b => b.MapFrom(src => src.IDStore));


            // Product

            CreateMap<Product, ProductViewModel>()
                .ForMember(a => a.ID, b => b.MapFrom(src => src.IDProduct));


            // Stock

            CreateMap<StockItem, StockItemViewModel>()
                .ForMember(a => a.ID, b => b.MapFrom(src => src.IDProduct))
                .ForMember(a => a.Nome, b => b.MapFrom(src => src.Product.Nome))
                .ForMember(a => a.SKU, b => b.MapFrom(src => src.Product.SKU))
                .ForMember(a => a.PrecoCusto, b => b.MapFrom(src => src.Product.PrecoCusto))
                .ForMember(a => a.PrecoVenda, b => b.MapFrom(src => src.PrecoVenda))
                .ForMember(a => a.Quantidade, b => b.MapFrom(src => src.Quantidade));

            CreateMap<StockItem, ProductViewModel>()
                .ForMember(a => a.ID, b => b.MapFrom(src => src.IDProduct))
                .ForMember(a => a.Nome, b => b.MapFrom(src => src.Product.Nome))
                .ForMember(a => a.SKU, b => b.MapFrom(src => src.Product.SKU))
                .ForMember(a => a.PrecoCusto, b => b.MapFrom(src => src.Product.PrecoCusto));

            CreateMap<Store, ProductsByStoreViewModel>()
                .ForMember(a => a.ID, b => b.MapFrom(src => src.IDStore))
                .ForMember(a => a.Nome, b => b.MapFrom(src => src.Nome))
                .ForMember(a => a.CNPJ, b => b.MapFrom(src => src.CNPJ))
                .ForMember(a => a.Endereco, b => b.MapFrom(src => $"{src.Logradouro}, {src.Numero}, {src.Bairro} - {src.Municipio}, {src.UF} - CEP: {src.CEP}"))
                .ForMember(a => a.DataInclusao, b => b.MapFrom(src => src.DataInclusao.ToShortDateString()))
                .ForMember(a => a.DataAlteracao, b => b.MapFrom(src => src.DataAlteracao.HasValue ? src.DataAlteracao.Value.ToShortDateString() : "----"))
                .ForMember(a => a.Products, b => b.MapFrom(src => src.StockItems));

            CreateMap<StockItem, StoresByProductViewModel>()
                .ForMember(a => a.ID, b => b.MapFrom(src => src.IDProduct))
                .ForMember(a => a.Nome, b => b.MapFrom(src => src.Product.Nome))
                .ForMember(a => a.SKU, b => b.MapFrom(src => src.Product.SKU))
                .ForMember(a => a.PrecoCusto, b => b.MapFrom(src => src.Product.PrecoCusto.ToString("C")))
                .ForMember(a => a.PrecoVenda, b => b.MapFrom(src => src.PrecoVenda.ToString("C")))
                .ForMember(a => a.Quantidade, b => b.MapFrom(src => src.Quantidade))
                .ForMember(a => a.Store, b => b.MapFrom(src => src.Store));

            CreateMap<StockItem, StoreViewModel>()
               .ForMember(a => a.ID, b => b.MapFrom(src => src.IDStore));
        }
    }
}