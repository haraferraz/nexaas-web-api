﻿using System;
using AutoMapper;
using Nexaas.Model.Models;
using Nexaas.BLL.ViewModels;

namespace Nexaas.BLL.Mappers
{
    public class ViewModelParaEntidadeMapping : Profile
    {
        public ViewModelParaEntidadeMapping()
        {
            // Store

            CreateMap<AddStoreViewModel, Store>()
                .ForMember(a => a.IDStore, b => b.MapFrom(src => Guid.NewGuid()))
                .ForMember(a => a.DataInclusao, b => b.MapFrom(src => DateTime.Now));

            CreateMap<EditStoreViewModel, Store>()
                .ForMember(a => a.IDStore, b => b.MapFrom(src => src.ID))
                .ForMember(a => a.DataAlteracao, b => b.MapFrom(src => DateTime.Now));

            CreateMap<RemoveStoreViewModel, Store>()
                .ForMember(a => a.IDStore, b => b.MapFrom(src => src.ID))
                .ForMember(a => a.DataAlteracao, b => b.MapFrom(src => DateTime.Now));


            // Product

            CreateMap<AddProductViewModel, Product>()
                .ForMember(a => a.IDProduct, b => b.MapFrom(src => Guid.NewGuid()))
                .ForMember(a => a.DataInclusao, b => b.MapFrom(src => DateTime.Now));

            CreateMap<EditProductViewModel, Product>()
                .ForMember(a => a.IDProduct, b => b.MapFrom(src => src.ID))
                .ForMember(a => a.DataAlteracao, b => b.MapFrom(src => DateTime.Now));

            CreateMap<RemoveProductViewModel, Product>()
                .ForMember(a => a.IDProduct, b => b.MapFrom(src => src.ID))
                .ForMember(a => a.DataAlteracao, b => b.MapFrom(src => DateTime.Now));


            // Stock

            CreateMap<AddStockItemViewModel, StockItem>()
                .ForMember(a => a.IDStockItem, b => b.MapFrom(src => Guid.NewGuid()));

            CreateMap<RemoveStockItemViewModel, StockItem>();
        }
    }
}