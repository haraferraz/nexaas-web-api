﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Nexaas.Model.Models;
using Nexaas.Model.UnitOfWork;
using Nexaas.BLL.ViewModels;
using Nexaas.Framework.Classes;
using AutoMapper;

namespace Nexaas.BLL.Logic
{
    public class StoreBLL : Base
    {
        public StoreBLL(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { }

        public async Task<StoreViewModel> Get(GetIDViewModel viewModel)
        {
            var model = await _unitOfWork.StoreRepository.Get(viewModel.ID);
            return _mapper.Map<Store, StoreViewModel>(model);
        }

        public async Task<IEnumerable<StoreViewModel>> GetList()
        {
            var model = await _unitOfWork.StoreRepository.GetList();
            return _mapper.Map<IEnumerable<Store>, IEnumerable<StoreViewModel>>(model);
        }

        public async Task<RetornoInsert> Add(AddStoreViewModel viewModel)
        {
            var retorno = new RetornoInsert();
            string cnpjFormatado = viewModel.CNPJ.Replace(".", "").Replace("/", "").Replace("-", "");
            var modelExistente = await _unitOfWork.StoreRepository.Get(cnpjFormatado);

            if (modelExistente != null)
            {
                retorno.Erro = true;
                retorno.CodigoHTTP = 401;
                retorno.Mensagem = "Já existe uma loja com o mesmo CNPJ";
            }
            else
            {
                try
                {
                    var model = _mapper.Map<AddStoreViewModel, Store>(viewModel);
                    await _unitOfWork.StoreRepository.Add(model, true);

                    retorno.ID = model.IDStore;
                    retorno.Erro = false;
                    retorno.CodigoHTTP = 200;
                    retorno.Mensagem = "Loja cadastrada com sucesso";
                }
                catch (Exception erro)
                {
                    retorno.Erro = true;
                    retorno.CodigoHTTP = 501;
                    retorno.Mensagem = $"Ocorreu um erro ao cadastrar a loja: {erro.Message}";
                }
            }

            return retorno;
        }

        public async Task<Retorno> Edit(EditStoreViewModel viewModel)
        {
            try
            {
                var model = _mapper.Map<EditStoreViewModel, Store>(viewModel);

                await _unitOfWork.StoreRepository.Edit(model, true,
                    a => a.IDStore,
                    a => a.DataInclusao);

                return new Retorno(200, "Loja alterada com sucesso", false);
            }
            catch (Exception erro)
            {
                return new Retorno(501, $"Ocorreu um erro ao alterar a loja: {erro.Message}", true);
            }   
        }

        public async Task<Retorno> Remove(RemoveStoreViewModel viewModel)
        {
            try
            {
                var model = await _unitOfWork.StoreRepository.Get(viewModel.ID);

                if (viewModel.SoftDelete)
                {
                    if (model != null)
                    {
                        model.Deleted = true;
                        model.DataAlteracao = DateTime.Now;

                        await _unitOfWork.StoreRepository.Edit(model, true,
                            a => a.IDStore,
                            a => a.DataInclusao);
                    }
                }
                else
                {
                    if (model != null)
                    {
                        await _unitOfWork.StockItemRepository.RemoveList(a => a.IDStore == model.IDStore);
                        await _unitOfWork.StoreRepository.Remove(model, true);
                    }   
                }

                return new Retorno(200, "Loja removida com sucesso", false);
            }
            catch (Exception erro)
            {
                return new Retorno(501, $"Ocorreu um erro ao remover a loja: {erro.Message}", true);
            }      
        }
    }
}