﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Nexaas.Model.Models;
using Nexaas.Model.UnitOfWork;
using Nexaas.BLL.ViewModels;
using Nexaas.Framework.Classes;
using AutoMapper;

namespace Nexaas.BLL.Logic
{
    public class StockItemBLL : Base
    {
        public StockItemBLL(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { }

        public async Task<StockItemViewModel> Get(GetStockItemViewModel viewModel)
        {
            var model = await _unitOfWork.StockItemRepository.Get(viewModel.IDStore.Value, viewModel.IDProduct.Value);
            return _mapper.Map<StockItem, StockItemViewModel>(model);
        }

        public async Task<StockItemViewModel> GetBySku(GetStockItemViewModel viewModel)
        {
            var model = await _unitOfWork.StockItemRepository.GetBySku(viewModel.SKU);
            return _mapper.Map<StockItem, StockItemViewModel>(model);
        }

        public async Task<IEnumerable<StockItemViewModel>> GetList()
        {
            var model = await _unitOfWork.StockItemRepository.GetList();
            return _mapper.Map<IEnumerable<StockItem>, IEnumerable<StockItemViewModel>>(model);
        }

        public async Task<ProductsByStoreViewModel> GetListByStore(GetStockItemViewModel viewModel)
        {
            var model = await _unitOfWork.StockItemRepository.GetListByStore(viewModel.IDStore.Value);
            return _mapper.Map<Store, ProductsByStoreViewModel>(model);
        }

        public async Task<IEnumerable<StoresByProductViewModel>> GetListByProduct(GetStockItemViewModel viewModel)
        {
            var model = await _unitOfWork.StockItemRepository.GetListByProduct(viewModel.IDProduct.Value);
            return _mapper.Map<IEnumerable<StockItem>, IEnumerable<StoresByProductViewModel>>(model);
        }

        public async Task<Retorno> Add(AddStockItemViewModel viewModel)
        {
            var retorno = new Retorno();

            try
            {
                var product = await _unitOfWork.ProductRepository.Get(viewModel.IDProduct.Value);
                var modelExistente = await _unitOfWork.StockItemRepository.Get(viewModel.IDStore.Value, viewModel.IDProduct.Value);
                var model = _mapper.Map<AddStockItemViewModel, StockItem>(viewModel);

                if (model.PrecoVenda < product.PrecoCusto)
                {
                    retorno.Erro = true;
                    retorno.CodigoHTTP = 400;
                    retorno.Mensagem = $"O preço de venda ({model.PrecoVenda}) não pode ser menor que o preço de custo ({product.PrecoCusto}).";
                }
                else if (modelExistente == null)
                {
                    await _unitOfWork.StockItemRepository.Add(model, true);

                    retorno.Erro = false;
                    retorno.CodigoHTTP = 200;
                    retorno.Mensagem = $"Produto inserido no estoque com sucesso";
                }
                else
                {
                    modelExistente.PrecoVenda = viewModel.PrecoVenda;
                    modelExistente.Quantidade = modelExistente.Quantidade + viewModel.Quantidade;

                    await _unitOfWork.StockItemRepository.Edit(modelExistente, true,
                        a => a.IDProduct,
                        a => a.IDStockItem,
                        a => a.IDStore);

                    retorno.Erro = false;
                    retorno.CodigoHTTP = 200;
                    retorno.Mensagem = $"Estoque do produto '{modelExistente.Product.Nome}' alterado com sucesso";
                }
            }
            catch (Exception erro)
            {
                retorno.Erro = true;
                retorno.CodigoHTTP = 501;
                retorno.Mensagem = $"Ocorreu um erro: {erro.Message}";
            }

            return retorno;
        }

        public async Task<Retorno> Remove(RemoveStockItemViewModel viewModel)
        {
            var retorno = new Retorno();

            try
            {
                var modelExistente = await _unitOfWork.StockItemRepository.Get(viewModel.IDStore.Value, viewModel.IDProduct.Value);
                
                if (modelExistente == null)
                {
                    retorno.Erro = true;
                    retorno.CodigoHTTP = 400;
                    retorno.Mensagem = $"Produto não encontrado no estoque.";
                }
                else
                {
                    if (viewModel.RemoverDoEstoque)
                    {
                        await _unitOfWork.StockItemRepository.Remove(modelExistente, true);

                        retorno.Erro = false;
                        retorno.CodigoHTTP = 200;
                        retorno.Mensagem = $"Produto '{modelExistente.Product.Nome}' removido do estoque com sucesso.";
                    }
                    else if (viewModel.Quantidade > modelExistente.Quantidade)
                    {
                        retorno.Erro = true;
                        retorno.CodigoHTTP = 400;
                        retorno.Mensagem = $"A quantidade informada ({viewModel.Quantidade}) é maior do que a disponível em estoque ({modelExistente.Quantidade}).";
                    }
                    else
                    {
                        modelExistente.Quantidade = modelExistente.Quantidade - viewModel.Quantidade;

                        await _unitOfWork.StockItemRepository.Edit(modelExistente, true,
                           a => a.IDProduct,
                           a => a.IDStockItem,
                           a => a.IDStore);

                        retorno.Erro = false;
                        retorno.CodigoHTTP = 200;
                        retorno.Mensagem = $"Estoque do produto '{modelExistente.Product.Nome}' alterado com sucesso.";
                    }
                }
            }
            catch (Exception erro)
            {
                retorno.Erro = true;
                retorno.CodigoHTTP = 501;
                retorno.Mensagem = $"Ocorreu um erro: {erro.Message}";
            }

            return retorno;
        }
    }
}