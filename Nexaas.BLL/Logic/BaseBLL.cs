﻿using Nexaas.Model.UnitOfWork;
using AutoMapper;

namespace Nexaas.BLL.Logic
{
    public class BaseBLL : Base
    {
        public BaseBLL(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { }
    }
}