﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Nexaas.Model.Models;
using Nexaas.Model.UnitOfWork;
using Nexaas.BLL.ViewModels;
using Nexaas.Framework.Classes;
using AutoMapper;

namespace Nexaas.BLL.Logic
{
    public class ProductBLL : Base
    {
        public ProductBLL(IUnitOfWork unitOfWork, IMapper mapper) : base(unitOfWork, mapper) { }

        public async Task<ProductViewModel> Get(GetIDViewModel viewModel)
        {
            var model = await _unitOfWork.ProductRepository.Get(viewModel.ID);
            return _mapper.Map<Product, ProductViewModel>(model);
        }

        public async Task<IEnumerable<ProductViewModel>> GetList()
        {
            var model = await _unitOfWork.ProductRepository.GetList();
            return _mapper.Map<IEnumerable<Product>, IEnumerable<ProductViewModel>>(model);
        }

        public async Task<RetornoInsert> Add(AddProductViewModel viewModel)
        {
            var retorno = new RetornoInsert();
            var modelExistente = await _unitOfWork.ProductRepository.Get(viewModel.SKU);

            if (modelExistente != null)
            {
                retorno.Erro = true;
                retorno.CodigoHTTP = 401;
                retorno.Mensagem = "Já existe um produto com o mesmo SKU";
            }
            else
            {
                try
                {
                    var model = _mapper.Map<AddProductViewModel, Product>(viewModel);
                    await _unitOfWork.ProductRepository.Add(model, true);

                    retorno.ID = model.IDProduct;
                    retorno.Erro = false;
                    retorno.CodigoHTTP = 200;
                    retorno.Mensagem = "Produto cadastrado com sucesso";
                }
                catch (Exception erro)
                {
                    retorno.Erro = true;
                    retorno.CodigoHTTP = 501;
                    retorno.Mensagem = $"Ocorreu um erro ao cadastrar o produto: {erro.Message}";
                }
            }

            return retorno;
        }

        public async Task<Retorno> Edit(EditProductViewModel viewModel)
        {
            try
            {
                var model = _mapper.Map<EditProductViewModel, Product>(viewModel);

                await _unitOfWork.ProductRepository.Edit(model, true,
                    a => a.IDProduct,
                    a => a.SKU,
                    a => a.DataInclusao);

                return new Retorno(200, "Produto alterado com sucesso", false);
            }
            catch (Exception erro)
            {
                return new Retorno(501, $"Ocorreu um erro ao alterar o produto: {erro.Message}", true);
            }   
        }

        public async Task<Retorno> Remove(RemoveProductViewModel viewModel)
        {
            try
            {
                var model = await _unitOfWork.ProductRepository.Get(viewModel.ID);

                if (viewModel.SoftDelete)
                {
                    if (model != null)
                    {
                        model.Deleted = true;
                        model.DataAlteracao = DateTime.Now;

                        await _unitOfWork.ProductRepository.Edit(model, true,
                            a => a.IDProduct,
                            a => a.SKU,
                            a => a.DataInclusao);
                    }
                }
                else
                {
                    if (model != null)
                    {
                        await _unitOfWork.StockItemRepository.RemoveList(a => a.IDProduct == model.IDProduct);
                        await _unitOfWork.ProductRepository.Remove(model, true);
                    }
                        
                }

                return new Retorno(200, "Produto removido com sucesso", false);
            }
            catch (Exception erro)
            {
                return new Retorno(501, $"Ocorreu um erro ao remover o produto: {erro.Message}", true);
            }      
        }
    }
}