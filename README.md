# Introdução #

Olá, bem-vindos ao meu projeto de teste para a vaga de desenvolvedor .NET!

### Tecnologias, técnicas e padrões utilizados ###

* .NET 5.0
* Entity Framework Core
* LINQ
* Bearer Token Authentication
* Unit of Work Pattern
* Repository Pattern
* SOLID
* SQL Server 2019
* Automapper
* Documentação em desenvolvimento com Swagger
* POO

### Como utilizar a WebAPI ###

* Link publicado de "produção": https://nexaas-webapi.ferraz.pro
* Versão atual: v1
* Todas as chamadas da API estão em coleções do Postman na área Downloads, assim como banco de dados, token já criado para testes e muito mais: https://bitbucket.org/haraferraz/nexaas-web-api/downloads

### Como é feita a autenticação ###

* É utilizado um token (Bearer);
* É possível controlar os acessos por funcionalidade (action/controller);
* É possível controlar os acessos por horário e data de início e fim;
* É necessário cadastrar uma Pessoa > Empresa > Sistema > SistemaChave e caso deseje, inserir as funcionalidades que a chave terá acesso na tabela SistemaChaveFuncionalidade (caso ela não tenha o flag AcessaTodasFuncionalidades = 1).


### Documentação Swagger em Desenvolvimento ###

<img src="https://i.ibb.co/6s8pJr8/swagger.png" />



Espero que gostem e estou à disposição para quaisquer eventuais dúvidas.