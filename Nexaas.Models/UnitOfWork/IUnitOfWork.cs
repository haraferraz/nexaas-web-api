﻿using Nexaas.Model.Repository;

namespace Nexaas.Model.UnitOfWork
{
    public interface IUnitOfWork
    {
        // Global

        ILogRepository LogAcessoRepository { get; set; }
        ISistemaRepository SistemaRepository { get; set; }
        ISistemaChaveRepository SistemaChaveRepository { get; set; }
        ISistemaChaveFuncionalidadeRepository SistemaChaveFuncionalidadeRepository { get; set; }
        ITokenRepository TokenRepository { get; set; }

        // Logic

        IStoreRepository StoreRepository { get; set; }
        IProductRepository ProductRepository { get; set; }
        IStockItemRepository StockItemRepository { get; set; }

        void Dispose();
        void Commit();
    }
}