﻿using Nexaas.Model.Repository;

namespace Nexaas.Model.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private Context _context { get; }

        // Global

        public ILogRepository LogAcessoRepository { get; set; }
        public ISistemaRepository SistemaRepository { get; set; }
        public ISistemaChaveRepository SistemaChaveRepository { get; set; }
        public ISistemaChaveFuncionalidadeRepository SistemaChaveFuncionalidadeRepository { get; set; }
        public ITokenRepository TokenRepository { get; set; }

        // Logic

        public IStoreRepository StoreRepository { get; set; }
        public IProductRepository ProductRepository { get; set; }
        public IStockItemRepository StockItemRepository { get; set; }

        public UnitOfWork(Context context)
        {
            _context = context;

            // Global

            LogAcessoRepository = new LogRepository(_context);
            SistemaRepository = new SistemaRepository(_context);
            SistemaChaveRepository = new SistemaChaveRepository(_context);
            SistemaChaveFuncionalidadeRepository = new SistemaChaveFuncionalidadeRepository(_context);
            TokenRepository = new TokenRepository(_context);

            // Logic

            StoreRepository = new StoreRepository(_context);
            ProductRepository = new ProductRepository(_context);
            StockItemRepository = new StockItemRepository(_context);
        }

        private bool _disposed;
        public void Commit()
        {
            _context.SaveChanges();
        }
        public void Dispose()
        {
            _context.Dispose();
        }
        private void Clear(bool disposing)
        {
            if (!_disposed)
            {
                if (disposing)
                    _context.Dispose();
            }
            _disposed = true;
        }
        ~UnitOfWork()
        {
            Clear(false);
        }
    }
}