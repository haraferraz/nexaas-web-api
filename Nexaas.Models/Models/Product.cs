﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Nexaas.Model.Models
{
    public class Product
    {
        [Key]
        public Guid IDProduct { get; set; }

        public string Nome { get; set; }

        public string SKU { get; set; }

        public decimal PrecoCusto { get; set; }

        public bool Deleted { get; set; }

        public DateTime DataInclusao { get; set; }

        public DateTime? DataAlteracao { get; set; }

        public virtual ICollection<StockItem> StockItems { get; set; }

        public virtual ICollection<ProductSale> Sales { get; set; }
    }
}