﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nexaas.Model.Models
{
    public class SistemaChave
    {
        public SistemaChave()
        {
            Tokens = new HashSet<Token>();
            LogsAcessos = new HashSet<LogAcesso>();
            SistemaChaveFuncionalidades = new HashSet<SistemaChaveFuncionalidade>();
        }

        [Key]
        public Guid IDSistemaChave { get; set; }
        public Guid IDSistema { get; set; }
        public string Chave { get; set; }
        public DateTime DataInicioValidade { get; set; }
        public DateTime DataFimValidade { get; set; }
        public TimeSpan HoraInicioPermissaoAcesso { get; set; }
        public TimeSpan HoraFimPermissaoAcesso { get; set; }
        public bool Ativo { get; set; }
        public bool AcessoTodasFuncionalidades { get; set; }
        public DateTime DataInclusao { get; set; }
        public DateTime? DataAlteracao { get; set; }

        [ForeignKey("IDSistema")]
        public virtual Sistema Sistema { get; set; }
        public virtual ICollection<SistemaChaveFuncionalidade> SistemaChaveFuncionalidades { get; set; }
        public virtual ICollection<Token> Tokens { get; set; }
        public virtual ICollection<LogAcesso> LogsAcessos { get; set; }
    }
}