﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nexaas.Model.Models
{
    public class LogAcesso
    {
        [Key]
        public Guid IDLogAcesso { get; set; }
        public Guid? IDSistemaChave { get; set; }
        public string IDCustomizado { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public DateTime DataHora { get; set; }
        public string IP { get; set; }
        public bool Permitido { get; set; }

        [ForeignKey("IDSistemaChave")]
        public virtual SistemaChave SistemaChave { get; set; }
    }
}