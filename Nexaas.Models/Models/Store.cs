﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Nexaas.Model.Models
{
    public class Store
    {
        [Key]
        public Guid IDStore { get; set; }

        [Required, MaxLength(14)]
        public string CNPJ { get; set; }

        [Required, MaxLength(150)]
        public string Nome { get; set; }

        [Required, MaxLength(8)]
        public string CEP { get; set; }

        [Required, MaxLength(150)]
        public string Logradouro { get; set; }

        public int? Numero { get; set; }

        [MaxLength(150)]
        public string Complemento { get; set; }

        [Required, MaxLength(50)]
        public string Bairro { get; set; }

        [Required, MaxLength(150)]
        public string Municipio { get; set; }

        [Required, MaxLength(2)]
        public string UF { get; set; }

        [Required]
        public bool Deleted { get; set; }

        [Required]
        public DateTime DataInclusao { get; set; }

        public DateTime? DataAlteracao { get; set; }

        public virtual ICollection<StockItem> StockItems { get; set; }
        public virtual ICollection<ProductSale> Sales { get; set; }
    }
}