﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Nexaas.Model.Models
{
    public class Funcionalidade
    {
        [Key]
        public Guid IDFuncionalidade { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }

        public virtual ICollection<SistemaChaveFuncionalidade> SistemasChavesFuncionalidades { get; set; }
    }
}