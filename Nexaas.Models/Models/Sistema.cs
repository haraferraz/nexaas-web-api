﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nexaas.Model.Models
{
    public class Sistema
    {
        public Sistema()
        {
            SistemaChaves = new HashSet<SistemaChave>();
        }

        [Key]
        public Guid IDSistema { get; set; }
        public Guid IDEmpresa { get; set; }
        public string Nome { get; set; }
        public bool Ativo { get; set; }
        public DateTime DataInclusao { get; set; }
        public DateTime? DataAlteracao { get; set; }

        public virtual ICollection<SistemaChave> SistemaChaves { get; set; }
    }
}