﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nexaas.Model.Models
{
    public class ProductSale
    {
        [Key]
        public Guid IDProductSale { get; set; }

        public Guid IDProduct { get; set; }

        public Guid IDStore { get; set; }

        public decimal CurrentCostPrice { get; set; }

        public decimal SalePrice { get; set; }

        public int Quantity { get; set; }

        public DateTime Date { get; set; }

        [ForeignKey("IDProduct")]
        public virtual Product Product { get; set; }

        [ForeignKey("IDStore")]
        public virtual Store Store { get; set; }
    }
}