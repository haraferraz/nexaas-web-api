﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nexaas.Model.Models
{
    public class StockItem
    {
        [Key]
        public Guid IDStockItem { get; set; }

        public Guid IDProduct { get; set; }

        public Guid IDStore { get; set; }

        public decimal PrecoVenda { get; set; }

        public int Quantidade { get; set; }

        [ForeignKey("IDProduct")]
        public virtual Product Product { get; set; }

        [ForeignKey("IDStore")]
        public virtual Store Store { get; set; }
    }
}