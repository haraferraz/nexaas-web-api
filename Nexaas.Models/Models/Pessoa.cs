﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Nexaas.Model.Models
{
    public class Pessoa
    {
        [Key]
        public Guid IDPessoa { get; set; }
        public string TipoPessoa { get; set; }
        public string CNPJ { get; set; }
        public string CPF { get; set; }
        public string NomeCompleto { get; set; }
        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public DateTime DataInclusao { get; set; }
        public Nullable<DateTime> DataAlteracao { get; set; }
    }
}