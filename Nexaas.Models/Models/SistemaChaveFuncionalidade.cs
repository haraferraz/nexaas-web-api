﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nexaas.Model.Models
{
    public class SistemaChaveFuncionalidade
    {
        [Key]
        public Guid IDSistemaChaveFuncionalidade { get; set; }
        public Guid IDSistemaChave { get; set; }
        public Guid IDFuncionalidade { get; set; }

        [ForeignKey("IDFuncionalidade")]
        public virtual Funcionalidade Funcionalidade { get; set; }

        [ForeignKey("IDSistemaChave")]
        public virtual SistemaChave SistemaChave { get; set; }
    }
}