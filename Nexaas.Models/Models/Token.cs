﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Nexaas.Model.Models
{
  public class Token
  {
    [Key]
    public Guid IDToken { get; set; }
    public Guid IDSistemaChave { get; set; }
    public string CodigoToken { get; set; }
    public DateTime DataHoraCriacao { get; set; }
    public DateTime? DataHoraExpiracao { get; set; }

    [ForeignKey("IDSistemaChave")]
    public virtual SistemaChave SistemaChave { get; set; }
  }
}