﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Nexaas.Model.Models;

namespace Nexaas.Model.Repository
{
    public class StoreRepository : RepositoryBase<Store>, IStoreRepository
    {
        public StoreRepository(Context context) : base(context) { }

        public async Task<Store> Get(Guid id)
        {
            return await _context.Stores
                .Include(a => a.StockItems)
                .FirstOrDefaultAsync(a => a.IDStore == id);
        }

        public async Task<Store> Get(string cnpj)
        {
            return await _context.Stores
                .AsNoTracking()
                .FirstOrDefaultAsync(a => a.CNPJ == cnpj);
        }

        public async Task<IEnumerable<Store>> GetList()
        {
            return await _context.Stores
                .AsNoTracking()
                .OrderBy(a => a.Nome)
                .ToListAsync();
        }
    }
}