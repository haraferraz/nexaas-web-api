﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Nexaas.Model.Models;

namespace Nexaas.Model.Repository
{
    public interface IStoreRepository : IRepositoryBase<Store> 
    {
        Task<Store> Get(Guid id);
        Task<Store> Get(string cnpj);
        Task<IEnumerable<Store>> GetList();
    }
}