﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using System.Collections.Generic;

namespace Nexaas.Model.Repository
{
    public interface IRepositoryBase<TEntity> where TEntity : class
    {
        Task Add(TEntity entity, bool save);
        Task AddList(List<TEntity> entities, bool save);
        Task Edit(TEntity entity, bool save, params Expression<Func<TEntity, object>>[] excludedProperties);
        Task Remove(TEntity entity, bool save);
        Task RemoveList(Expression<Func<TEntity, bool>> filter = null);
        IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null);
        Task Save();
    }
}