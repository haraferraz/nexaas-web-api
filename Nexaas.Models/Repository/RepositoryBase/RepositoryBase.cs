﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace Nexaas.Model.Repository
{
    public class RepositoryBase<TEntity> : IDisposable, IRepositoryBase<TEntity> where TEntity : class
    {
        protected Context _context;
        private DbSet<TEntity> DbSet;

        public RepositoryBase(Context context)
        {
            _context = context;
            DbSet = context.Set<TEntity>();
        }

        public async Task Save()
        {
            await _context.SaveChangesAsync();
        }

        public async Task Add(TEntity entity, bool save)
        {
            await DbSet.AddAsync(entity);

            if (save)
                await Save();
        }

        public async Task Edit(TEntity entity, bool save, params Expression<Func<TEntity, object>>[] excludedProperties)
        {
            var entry = _context.Entry(entity);
            entry.State = EntityState.Modified;

            if (excludedProperties.Any())
                foreach (var property in excludedProperties)
                    entry.Property(property).IsModified = false;

            if (save)
                await Save();
        }

        public async Task Remove(TEntity entity, bool save)
        {
            if (_context.Entry(entity).State == EntityState.Detached)
                DbSet.Attach(entity);

            DbSet.Remove(entity);

            if (save)
                await Save();
        }

        public async Task RemoveList(Expression<Func<TEntity, bool>> filter = null)
        {
            var query = DbSet;
            var listDelete = query.Where(filter).ToList();

            foreach (var item in listDelete)
                await Task.Run(() => DbSet.Remove(item));
        }

        public async Task AddList(List<TEntity> entities, bool save)
        {
            foreach (var item in entities)
                DbSet.Add(item);

            if (save)
                await Save();
        }

        public virtual IQueryable<TEntity> Get(Expression<Func<TEntity, bool>> filter = null, Func<IQueryable<TEntity>, IOrderedQueryable<TEntity>> orderBy = null)
        {
            IQueryable<TEntity> query = DbSet;

            if (filter != null)
                query = query.Where(filter);

            if (orderBy != null)
                return orderBy(query);
            else return query;
        }

        public void Dispose()
        {
            DbSet = null;
            _context.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}