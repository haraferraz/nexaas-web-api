﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Nexaas.Model.Models;

namespace Nexaas.Model.Repository
{
    public class ProductRepository : RepositoryBase<Product>, IProductRepository
    {
        public ProductRepository(Context context) : base(context) { }

        public async Task<Product> Get(Guid id)
        {
            return await _context.Products
                .FirstOrDefaultAsync(a => a.IDProduct == id);
        }

        public async Task<Product> Get(string sku)
        {
            return await _context.Products
                .AsNoTracking()
                .FirstOrDefaultAsync(a => a.SKU == sku);
        }

        public async Task<IEnumerable<Product>> GetList()
        {
            return await _context.Products
                .AsNoTracking()
                .OrderBy(a => a.Nome)
                .ToListAsync();
        }
    }
}