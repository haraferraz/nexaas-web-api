﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Nexaas.Model.Models;

namespace Nexaas.Model.Repository
{
    public interface IProductRepository : IRepositoryBase<Product> 
    {
        Task<Product> Get(Guid id);
        Task<Product> Get(string sku);
        Task<IEnumerable<Product>> GetList();
    }
}