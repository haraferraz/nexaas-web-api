﻿using Nexaas.Model.Models;

namespace Nexaas.Model.Repository
{
    public class SistemaRepository : RepositoryBase<Sistema>, ISistemaRepository
    {
        public SistemaRepository(Context context) : base(context) { }
    }
}