﻿using System;
using System.Threading.Tasks;
using Nexaas.Model.Models;

namespace Nexaas.Model.Repository
{
    public interface ISistemaRepository : IRepositoryBase<Sistema>
    {
    }
}