﻿using System;
using System.Threading.Tasks;
using Nexaas.Model.Models;

namespace Nexaas.Model.Repository
{
    public interface ILogRepository : IDisposable
    {
        Task SalvarLog(LogAcesso model);
    }
}