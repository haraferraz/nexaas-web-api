﻿using System;
using System.Threading.Tasks;
using Nexaas.Model.Models;

namespace Nexaas.Model.Repository
{
    public interface IAutenticacaoRepository : IDisposable
    {
        Task<SistemaChave> BuscarChave(string chave);
        Task SalvarToken(Token model);
        Task<Token> BuscarToken(string token);
    }
}