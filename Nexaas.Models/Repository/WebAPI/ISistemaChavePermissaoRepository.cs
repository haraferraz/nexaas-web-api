﻿using System;
using System.Threading.Tasks;
using Nexaas.Framework.Classes;

namespace Nexaas.Model.Repository
{
    public interface ISistemaChavePermissaoRepository : IDisposable
    {
        Task<RetornoPermissao> BuscarChavePermissao(string token, string action, string controller);
    }
}