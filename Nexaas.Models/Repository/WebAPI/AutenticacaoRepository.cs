﻿using System;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Nexaas.Model.Models;
using System.Linq;

namespace Nexaas.Model.Repository
{
    public class AutenticacaoRepository : IAutenticacaoRepository
    {
        private readonly Context _contexto;

        public AutenticacaoRepository(Context contexto)
        {
            _contexto = contexto;
        }

        #region Queries Pré-Compiladas

        private static readonly Func<Context, string, Task<SistemaChave>> _buscarChave =
          EF.CompileAsyncQuery((Context contexto, string chave) =>
            contexto.SistemaChaves
              .Include(a => a.Sistema)
              .Where(a => a.Chave.Equals(chave))
              .AsNoTracking()
              .FirstOrDefault());

        private static readonly Func<Context, string, Task<Token>> _buscarToken =
          EF.CompileAsyncQuery((Context contexto, string token) =>
            contexto.Tokens
              .Where(a => a.CodigoToken.Equals(token))
              .AsNoTracking()
              .FirstOrDefault());

        #endregion

        public async Task<SistemaChave> BuscarChave(string chave)
        {
            return await _buscarChave(_contexto, chave);
        }
        public async Task SalvarToken(Token model)
        {
            await _contexto.Tokens.AddAsync(model);
            await _contexto.SaveChangesAsync();
        }
        public async Task<Token> BuscarToken(string token)
        {
            return await _buscarToken(_contexto, token);
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _contexto.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}