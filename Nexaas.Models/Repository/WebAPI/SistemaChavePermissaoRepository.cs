﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Nexaas.Model.Models;
using Nexaas.Framework.Classes;

namespace Nexaas.Model.Repository
{
  public class SistemaChavePermissaoRepository : ISistemaChavePermissaoRepository
  {
    private readonly Context _contexto;

    public SistemaChavePermissaoRepository(Context contexto)
    {
      _contexto = contexto;
    }

    #region Queries Pré-Compiladas

    private static readonly Func<Context, string, Task<Token>> _buscarChavePorToken =
      EF.CompileAsyncQuery((Context contexto, string token) =>
        contexto.Tokens
          .Include(a => a.SistemaChave)
          .Where(a => a.CodigoToken.Equals(token.Replace("Bearer ", "")))
          .AsNoTracking()
          .FirstOrDefault());

    #endregion

    public async Task<RetornoPermissao> BuscarChavePermissao(string token, string action, string controller)
    {
      var retorno = new RetornoPermissao()
      {
        IDSistemaChave = Guid.Empty,
        Permitido = false
      };

      var chave = await _buscarChavePorToken(_contexto, token);
      
      if (chave != null)
      {
        retorno.IDSistemaChave = chave.IDSistemaChave;

        bool acessoFuncionalidade = _contexto.SistemaChaveFuncionalidades
         .Include(a => a.Funcionalidade)
         .FirstOrDefault(a => a.IDSistemaChave == chave.IDSistemaChave &&
                              a.Funcionalidade.Action.Equals(action) &&
                              a.Funcionalidade.Controller.Equals(controller) &&
                              a.SistemaChave.Ativo) != null || chave.SistemaChave.AcessoTodasFuncionalidades;

        retorno.Permitido = acessoFuncionalidade || chave.SistemaChave.AcessoTodasFuncionalidades;
      }
     
      return retorno;
    }

    private bool disposed = false;
    protected virtual void Dispose(bool disposing)
    {
      if (!this.disposed)
      {
        if (disposing)
        {
          _contexto.Dispose();
        }
      }
      this.disposed = true;
    }
    public void Dispose()
    {
      Dispose(true);
      GC.SuppressFinalize(this);
    }
  }
}