﻿using System;
using System.Threading.Tasks;
using Nexaas.Model.Models;

namespace Nexaas.Model.Repository
{
    public class LogRepository : ILogRepository
    {
        private readonly Context _contexto;

        public LogRepository(Context contexto)
        {
            _contexto = contexto;
        }

        public async Task SalvarLog(LogAcesso model)
        {
            await _contexto.LogAcessos.AddAsync(model);
            await _contexto.SaveChangesAsync();
        }

        private bool disposed = false;
        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _contexto.Dispose();
                }
            }
            this.disposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}