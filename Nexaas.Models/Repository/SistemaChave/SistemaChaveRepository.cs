﻿using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Nexaas.Model.Models;

namespace Nexaas.Model.Repository
{
    public class SistemaChaveRepository : RepositoryBase<SistemaChave>, ISistemaChaveRepository
    {
        public SistemaChaveRepository(Context context) : base(context) { }

        public async Task<SistemaChave> BuscarChave(string chave)
        {
            return await _context.SistemaChaves
              .AsNoTracking()
              .Include(a => a.Sistema)
              .FirstOrDefaultAsync(a => a.Chave.Equals(chave));
        }
    }
}