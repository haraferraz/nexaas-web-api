﻿using System.Threading.Tasks;
using Nexaas.Model.Models;

namespace Nexaas.Model.Repository
{
    public interface ISistemaChaveRepository : IRepositoryBase<SistemaChave>
    {
        Task<SistemaChave> BuscarChave(string chave);
    }
}