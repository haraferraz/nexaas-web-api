﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Nexaas.Model;
using Nexaas.Model.Models;

namespace Nexaas.Model.Repository
{
    public class TokenRepository : RepositoryBase<Token>, ITokenRepository
    {
        public TokenRepository(Context context) : base(context) { }

        #region Queries Pré-Compiladas

        private static readonly Func<Context, string, Task<Token>> _buscarToken =
          EF.CompileAsyncQuery((Context contexto, string token) =>
            contexto.Tokens
              .Where(a => a.CodigoToken.Equals(token))
              .AsNoTracking()
              .FirstOrDefault());

        #endregion

        public async Task<Token> BuscarToken(string token)
        {
            return await _buscarToken(_context, token);
        }
    }
}