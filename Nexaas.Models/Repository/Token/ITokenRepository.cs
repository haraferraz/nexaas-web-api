﻿using System.Threading.Tasks;
using Nexaas.Model.Models;

namespace Nexaas.Model.Repository
{
    public interface ITokenRepository : IRepositoryBase<Token>
    {
        Task<Token> BuscarToken(string token);
    }
}