﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Nexaas.Model.Models;
using Nexaas.Framework.Classes;

namespace Nexaas.Model.Repository
{
    public class SistemaChaveFuncionalidadeRepository : RepositoryBase<SistemaChaveFuncionalidade>, ISistemaChaveFuncionalidadeRepository
    {
        public SistemaChaveFuncionalidadeRepository(Context context) : base(context) { }

        #region Queries Pré-Compiladas

        private static readonly Func<Context, string, Task<Token>> _buscarChavePorToken =
          EF.CompileAsyncQuery((Context contexto, string token) =>
            contexto.Tokens
              .Include(a => a.SistemaChave)
              .Where(a => a.CodigoToken.Equals(token.Replace("Bearer ", "")))
              .AsNoTracking()
              .FirstOrDefault());

        #endregion

        public async Task<RetornoPermissao> BuscarChavePermissao(string token, string action, string controller)
        {
            var retorno = new RetornoPermissao()
            {
                IDSistemaChave = Guid.Empty,
                Permitido = false
            };

            var chave = await _buscarChavePorToken(_context, token);

            if (chave != null)
            {
                retorno.IDSistemaChave = chave.IDSistemaChave;

                bool acessoFuncionalidade = _context.SistemaChaveFuncionalidades
                 .AsNoTracking()
                 .Include(a => a.Funcionalidade)
                 .Where(a => a.IDSistemaChave == chave.IDSistemaChave &&
                             a.Funcionalidade.Action.Equals(action) &&
                             a.Funcionalidade.Controller.Equals(controller) &&
                             a.SistemaChave.Ativo) != null || chave.SistemaChave.AcessoTodasFuncionalidades;

                retorno.Permitido = acessoFuncionalidade || chave.SistemaChave.AcessoTodasFuncionalidades;
            }

            return retorno;
        }
    }
}