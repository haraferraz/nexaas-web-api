﻿using System.Threading.Tasks;
using Nexaas.Model.Models;
using Nexaas.Framework.Classes;

namespace Nexaas.Model.Repository
{
    public interface ISistemaChaveFuncionalidadeRepository : IRepositoryBase<SistemaChaveFuncionalidade>
    {
        Task<RetornoPermissao> BuscarChavePermissao(string token, string action, string controller);
    }
}