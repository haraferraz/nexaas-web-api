﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Nexaas.Model.Models;

namespace Nexaas.Model.Repository
{
    public class StockItemRepository : RepositoryBase<StockItem>, IStockItemRepository
    {
        public StockItemRepository(Context context) : base(context) { }

        public async Task<StockItem> Get(Guid idStore, Guid idProduct)
        {
            return await _context.StockItems
                .AsNoTracking()
                .Include(a => a.Store)
                .Include(a => a.Product)
                .FirstOrDefaultAsync(a => a.IDStore == idStore && a.IDProduct == idProduct);
        }

        public async Task<StockItem> GetBySku(string sku)
        {
            return await _context.StockItems
                .AsNoTracking()
                .Include(a => a.Store)
                .Include(a => a.Product)
                .FirstOrDefaultAsync(a => a.Product.SKU == sku);
        }

        public async Task<IEnumerable<StockItem>> GetList()
        {
            return await _context.StockItems
                .AsNoTracking()
                .Include(a => a.Store)
                .Include(a => a.Product)
                .OrderBy(a => a.Product.Nome)
                .ToListAsync();
        }

        public async Task<Store> GetListByStore(Guid idStore)
        {
            return await _context.Stores
                .AsNoTracking()
                .Include(a => a.StockItems).ThenInclude(a => a.Product)
                .FirstOrDefaultAsync(a => a.IDStore == idStore);
        }

        public async Task<IEnumerable<StockItem>> GetListByProduct(Guid idProduct)
        {
            return await _context.StockItems
                .AsNoTracking()
                .Include(a => a.Store)
                .Include(a => a.Product)
                .Where(a => a.IDProduct == idProduct)
                .ToListAsync();
        }
    }
}