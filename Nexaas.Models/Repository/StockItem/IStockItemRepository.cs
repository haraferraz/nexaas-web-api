﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Nexaas.Model.Models;

namespace Nexaas.Model.Repository
{
    public interface IStockItemRepository : IRepositoryBase<StockItem>
    {
        Task<StockItem> Get(Guid idStore, Guid idProduct);
        Task<StockItem> GetBySku(string sku);
        Task<IEnumerable<StockItem>> GetList();
        Task<Store> GetListByStore(Guid idStore);
        Task<IEnumerable<StockItem>> GetListByProduct(Guid idProduct);
    }
}