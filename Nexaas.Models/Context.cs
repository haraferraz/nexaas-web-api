﻿using Microsoft.EntityFrameworkCore;
using Nexaas.Model.Models;

namespace Nexaas.Model
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options) { }

        public DbSet<Funcionalidade> Funcionalidades { get; set; }
        public DbSet<LogAcesso> LogAcessos { get; set; }
        public DbSet<Sistema> Sistemas { get; set; }
        public DbSet<SistemaChave> SistemaChaves { get; set; }
        public DbSet<SistemaChaveFuncionalidade> SistemaChaveFuncionalidades { get; set; }
        public DbSet<Token> Tokens { get; set; }

        public DbSet<Store> Stores { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<ProductSale> ProductSales { get; set; }
        public DbSet<StockItem> StockItems { get; set; }
        
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Funcionalidade>().ToTable("Funcionalidade");
            modelBuilder.Entity<LogAcesso>().ToTable("LogAcesso");
            modelBuilder.Entity<Sistema>().ToTable("Sistema");
            modelBuilder.Entity<SistemaChave>().ToTable("SistemaChave");
            modelBuilder.Entity<SistemaChaveFuncionalidade>().ToTable("SistemaChaveFuncionalidade");
            modelBuilder.Entity<Token>().ToTable("Token");

            modelBuilder.Entity<Store>().ToTable("Store");
            modelBuilder.Entity<Product>().ToTable("Product");
            modelBuilder.Entity<ProductSale>().ToTable("ProductSale");
            modelBuilder.Entity<StockItem>().ToTable("StockItem");
        }
    }
}