﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Nexaas.Model;
using Nexaas.Model.Models;
using Nexaas.Model.Repository;

namespace Nexaas.Web.API.Filters
{
    public class VerificarPermissaoActionFilter : ActionFilterAttribute
    {
        private readonly ILogRepository logRepository;
        private readonly ISistemaChavePermissaoRepository _sistemaChavePermissaoRepository;

        public VerificarPermissaoActionFilter(Context contexto)
        {
            logRepository = new LogRepository(contexto);
            _sistemaChavePermissaoRepository = new SistemaChavePermissaoRepository(contexto);
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {
            string token = context.HttpContext.Request.Headers["Authorization"];
            string action = context.RouteData.Values["action"] != null ? context.RouteData.Values["action"].ToString() : string.Empty;
            string controller = context.RouteData.Values["controller"] != null ? context.RouteData.Values["controller"].ToString() : string.Empty;

            if (!controller.Equals("webhook", StringComparison.InvariantCultureIgnoreCase))
            {
                int? codigoCliente = null;

                if (context.RouteData.Values["codigoCliente"] != null)
                    codigoCliente = int.Parse(context.RouteData.Values["codigoCliente"].ToString());

                if (!controller.Equals("Token") && !controller.Equals("Notificacao"))
                {
                    if (string.IsNullOrEmpty(token) || string.IsNullOrEmpty(action) || string.IsNullOrEmpty(controller))
                    {
                        context.Result = new ContentResult
                        {
                            StatusCode = 204,
                            Content = "Ocorreu um erro ao tentar verificar as permissões desta chave."
                        };
                    }
                    else
                    {
                        var retornoPermissao = _sistemaChavePermissaoRepository
                          .BuscarChavePermissao(token.Replace("Bearer ", ""), action, controller).Result;

                        if (!retornoPermissao.Permitido)
                            context.Result = new ContentResult
                            {
                                StatusCode = 401,
                                Content = "O token e chave informados não possui acesso ao serviço solicitado. Entre em contato com o suporte do Cronos ERP."
                            };

                        if (retornoPermissao.IDSistemaChave != Guid.Empty)
                        {
                            var logAcesso = new LogAcesso()
                            {
                                IDLogAcesso = Guid.NewGuid(),
                                IDSistemaChave = retornoPermissao.IDSistemaChave,
                                Action = action,
                                Controller = controller,
                                DataHora = DateTime.Now,
                                IP = context.HttpContext.Connection.RemoteIpAddress.ToString(),
                                Permitido = retornoPermissao.Permitido
                            };

                            logRepository.SalvarLog(logAcesso);
                        }
                    }
                }
            }

            base.OnActionExecuting(context);
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);
        }
    }
}