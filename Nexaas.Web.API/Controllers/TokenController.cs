﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Authorization;
using Swashbuckle.AspNetCore.Annotations;
using Nexaas.BLL.Classes;
using Nexaas.Framework.Classes;
using Nexaas.Web.API.Authentication;
using Nexaas.Model.UnitOfWork;
using Nexaas.Model.Models;
using AutoMapper;

namespace Nexaas.Web.API.Controllers
{
    [ApiController, ApiVersion("1")]
    [Route("v{version:apiVersion}/token"), AllowAnonymous]
    [ApiExplorerSettings(GroupName = "authentication")]
    public class TokenController : BaseController
    {
        public TokenController(IUnitOfWork unitOfWork, IMapper mapper, IOptions<Mensagens> mensagens, IOptions<AppSettings> appSettings) :
          base(unitOfWork, mapper, mensagens, appSettings)
        { }

        /// <summary>
        /// Gera um novo token baseado em uma chave de acesso
        /// </summary>
        /// <param name="chave">Chave de acesso</param>
        /// <response code="200">Token gerado com sucesso</response>
        /// <response code="400">Caso ocorra algum problema com os parâmetros informados</response>
        /// <response code="401">Usuário e/ou senha inválidos</response>
        /// <response code="412">Usuário e/ou não informados</response>
        [SwaggerOperation(Tags = new[] { "Token" })]
        [HttpPost, ProducesResponseType(200), ProducesResponseType(204), ProducesResponseType(400)]
        public async Task<IActionResult> Create([FromHeader] string chave)
        {
            if (string.IsNullOrEmpty(chave))
                return StatusCode(412, new Retorno(412, "Chacve não informada", true));

            var chaveSistema = await _unitOfWork.SistemaChaveRepository.BuscarChave(chave);
            bool gerarToken = false;

            if (chaveSistema != null && chaveSistema.Ativo && chaveSistema.Sistema.Ativo)
            {
                var dataAtual = DateTime.Now;

                if (dataAtual >= chaveSistema.DataInicioValidade && dataAtual <= chaveSistema.DataFimValidade)
                    gerarToken = true;
            }

            if (gerarToken)
            {
                string nomeSistema = chaveSistema.Sistema.Nome;

                var token = new JwtTokenBuilder()
                  .AddSecurityKey(JwtSecurityKey.Create(_appSettings.ChavePrivadaApi))
                  .AddSubject(nomeSistema)
                  .AddIssuer(_appSettings.TokenIssuer)
                  .AddAudience(_appSettings.TokenAudience)
                  .AddClaim("Name", nomeSistema)
                  .AddClaim("PrimarySid", chaveSistema.IDSistema.ToString())
                  .AddClaim("Sid", chaveSistema.IDSistemaChave.ToString())
                  .AddExpiry(int.Parse(_appSettings.ValidadeToken.ToString()))
                  .Build();

                var novoToken = new Token()
                {
                    IDToken = Guid.NewGuid(),
                    IDSistemaChave = chaveSistema.IDSistemaChave,
                    CodigoToken = token.Token,
                    DataHoraCriacao = DateTime.Now,
                    DataHoraExpiracao = token.ValidoAte
                };

                await _unitOfWork.TokenRepository.Add(novoToken, true);

                return StatusCode(200, token);
            }

            return StatusCode(401, new Retorno(401, "Acesso negado para a chave de acesso informada", true));
        }
    }
}