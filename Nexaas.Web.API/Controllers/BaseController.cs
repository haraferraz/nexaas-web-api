﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Nexaas.BLL.Classes;
using Nexaas.Model.UnitOfWork;
using AutoMapper;

namespace Nexaas.Web.API.Controllers
{
    [ApiController, ApiVersion("1")]
    public class BaseController : Controller
    {
        protected readonly IMapper _mapper;
        protected readonly Mensagens _mensagens;
        protected readonly IUnitOfWork _unitOfWork;
        protected readonly AppSettings _appSettings;

        public BaseController(IUnitOfWork unitOfWork, IMapper mapper, IOptions<Mensagens> mensagens, IOptions<AppSettings> appSettings)
        {
            _mapper = mapper;
            _unitOfWork = unitOfWork;
            _mensagens = mensagens.Value;
            _appSettings = appSettings.Value;
        }
    }
}