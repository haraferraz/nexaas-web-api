﻿using System;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Nexaas.BLL.ViewModels;
using Nexaas.Framework.Classes;
using Nexaas.BLL.Logic;
using Nexaas.Model.UnitOfWork;
using Nexaas.BLL.Classes;
using Swashbuckle.AspNetCore.Annotations;
using Newtonsoft.Json;
using AutoMapper;

namespace Nexaas.Web.API.Controllers
{
    [ApiController, ApiVersion("1")]
    [Route("v{version:apiVersion}/stock")]
    [ApiExplorerSettings(GroupName = "stock")]
    public class StockItemController : BaseController
    {
        private readonly StockItemBLL _stockItemBLL;
        public StockItemController(IUnitOfWork unitOfWork, IMapper mapper, IOptions<Mensagens> mensagens, IOptions<AppSettings> appSettings) :
          base(unitOfWork, mapper, mensagens, appSettings)
        {
            _stockItemBLL = new StockItemBLL(unitOfWork, mapper);
        }

        /// <summary>
        /// Obtém um produto específico em estoque de acordo com o ID do produto e ID da loja informados
        /// </summary>
        /// <param name="viewModel">Classe que recebe o ID do produto e ID da loja a serem consultados</param>
        /// <response code="200">Estoque retornado com sucesso</response>
        /// <response code="204">Nenhum conteúdo informado com os parâmetros informados</response>
        /// <response code="400">Caso ocorra algum problema com os parâmetros informados</response>
        /// <response code="403">Chave de acesso (token) inválida, não informada ou sem permissão para acessar esta funcionalidade</response>
        /// <response code="500">Erro interno do servidor</response>
        /// <response code="502">Bad Gateway. Provavelmente um dos parâmetros estão incorretos</response>
        [HttpGet]
        [SwaggerOperation(Tags = new[] { "Stock" })]
        [ProducesResponseType(200), ProducesResponseType(204), ProducesResponseType(400), ProducesResponseType(403), ProducesResponseType(500), ProducesResponseType(502)]
        public async Task<IActionResult> Get([FromBody, Required] GetStockItemViewModel viewModel)
        {
            try
            {
                var retornoBLL = await _stockItemBLL.Get(viewModel);
                return StatusCode(200, retornoBLL);
            }
            catch (Exception erro)
            {
                return StatusCode(501, new Retorno(501, erro.Message, true));
            }
        }

        /// <summary>
        /// Obtém o produto em estoque por seu SKU
        /// </summary>
        /// <param name="viewModel">Classe que recebe o SKU do produto a ser consultado</param>
        /// <response code="200">Produto retornado com sucesso</response>
        /// <response code="204">Nenhum conteúdo informado com os parâmetros informados</response>
        /// <response code="400">Caso ocorra algum problema com os parâmetros informados</response>
        /// <response code="403">Chave de acesso (token) inválida, não informada ou sem permissão para acessar esta funcionalidade</response>
        /// <response code="500">Erro interno do servidor</response>
        /// <response code="502">Bad Gateway. Provavelmente um dos parâmetros estão incorretos</response>
        [HttpGet, Route("sku")]
        [SwaggerOperation(Tags = new[] { "Stock" })]
        [ProducesResponseType(200), ProducesResponseType(204), ProducesResponseType(400), ProducesResponseType(403), ProducesResponseType(500), ProducesResponseType(502)]
        public async Task<IActionResult> GetBySku([FromBody, Required] GetStockItemViewModel viewModel)
        {
            if (string.IsNullOrEmpty(viewModel.SKU))
                return StatusCode(400, new Retorno(400, "Informe o SKU do produto.", true));

            try
            {
                var retornoBLL = await _stockItemBLL.GetBySku(viewModel);
                return StatusCode(200, retornoBLL);
            }
            catch (Exception erro)
            {
                return StatusCode(501, new Retorno(501, erro.Message, true));
            }
        }

        /// <summary>
        /// Obtém a lista de produtos em estoque
        /// </summary>
        /// <response code="200">Lista retornada com sucesso</response>
        /// <response code="204">Nenhum conteúdo informado com os parâmetros informados</response>
        /// <response code="400">Caso ocorra algum problema com os parâmetros informados</response>
        /// <response code="403">Chave de acesso (token) inválida, não informada ou sem permissão para acessar esta funcionalidade</response>
        /// <response code="500">Erro interno do servidor</response>
        /// <response code="502">Bad Gateway. Provavelmente um dos parâmetros estão incorretos</response>
        [HttpGet, Route("list")]
        [SwaggerOperation(Tags = new[] { "Stock" })]
        [ProducesResponseType(200), ProducesResponseType(204), ProducesResponseType(400), ProducesResponseType(403), ProducesResponseType(500), ProducesResponseType(502)]
        public async Task<IActionResult> GetList()
        {
            try
            {
                var retornoBLL = await _stockItemBLL.GetList();
                return StatusCode(200, retornoBLL);
            }
            catch (Exception erro)
            {
                return StatusCode(501, new Retorno(501, erro.Message, true));
            }
        }

        /// <summary>
        /// Obtém a lista de produtos em estoque por ID da loja
        /// </summary>
        /// <param name="viewModel">Classe que recebe o ID da loja a ser consultada</param>
        /// <response code="200">Lista retornada com sucesso</response>
        /// <response code="204">Nenhum conteúdo informado com os parâmetros informados</response>
        /// <response code="400">Caso ocorra algum problema com os parâmetros informados</response>
        /// <response code="403">Chave de acesso (token) inválida, não informada ou sem permissão para acessar esta funcionalidade</response>
        /// <response code="500">Erro interno do servidor</response>
        /// <response code="502">Bad Gateway. Provavelmente um dos parâmetros estão incorretos</response>
        [HttpGet, Route("list-by-store")]
        [SwaggerOperation(Tags = new[] { "Stock" })]
        [ProducesResponseType(200), ProducesResponseType(204), ProducesResponseType(400), ProducesResponseType(403), ProducesResponseType(500), ProducesResponseType(502)]
        public async Task<IActionResult> GetListByStore([FromBody, Required] GetStockItemViewModel viewModel)
        {
            if (!viewModel.IDStore.HasValue)
                return StatusCode(400, new Retorno(400, "Informe o ID da loja.", true));

            try
            {
                var retornoBLL = await _stockItemBLL.GetListByStore(viewModel);
                return StatusCode(200, retornoBLL);
            }
            catch (Exception erro)
            {
                return StatusCode(501, new Retorno(501, erro.Message, true));
            }
        }

        /// <summary>
        /// Obtém a lista de produtos em estoque por ID do produto
        /// </summary>
        /// <param name="viewModel">Classe que recebe o ID do produto a ser consultado</param>
        /// <response code="200">Lista retornada com sucesso</response>
        /// <response code="204">Nenhum conteúdo informado com os parâmetros informados</response>
        /// <response code="400">Caso ocorra algum problema com os parâmetros informados</response>
        /// <response code="403">Chave de acesso (token) inválida, não informada ou sem permissão para acessar esta funcionalidade</response>
        /// <response code="500">Erro interno do servidor</response>
        /// <response code="502">Bad Gateway. Provavelmente um dos parâmetros estão incorretos</response>
        [HttpGet, Route("list-by-product")]
        [SwaggerOperation(Tags = new[] { "Stock" })]
        [ProducesResponseType(200), ProducesResponseType(204), ProducesResponseType(400), ProducesResponseType(403), ProducesResponseType(500), ProducesResponseType(502)]
        public async Task<IActionResult> GetListByProduct([FromBody, Required] GetStockItemViewModel viewModel)
        {
            if (!viewModel.IDProduct.HasValue)
                return StatusCode(400, new Retorno(400, "Informe o ID do produto.", true));

            try
            {
                var retornoBLL = await _stockItemBLL.GetListByProduct(viewModel);
                return StatusCode(200, retornoBLL);
            }
            catch (Exception erro)
            {
                return StatusCode(501, new Retorno(501, erro.Message, true));
            }
        }

        /// <summary>
        /// Insere um produto em estoque ou altera o estoque de um produto existente
        /// </summary>
        /// <param name="viewModel">Classe que recebe os dados do produto a ser cadastrado/alterado</param>
        /// <response code="200">Produto cadastrado/alterado com sucesso</response>
        /// <response code="204">Nenhum conteúdo informado com os parâmetros informados</response>
        /// <response code="400">Caso ocorra algum problema com os parâmetros informados</response>
        /// <response code="403">Chave de acesso (token) inválida, não informada ou sem permissão para acessar esta funcionalidade</response>
        /// <response code="500">Erro interno do servidor</response>
        /// <response code="502">Bad Gateway. Provavelmente um dos parâmetros estão incorretos</response>
        [HttpPost]
        [SwaggerOperation(Tags = new[] { "Stock" })]
        [ProducesResponseType(200), ProducesResponseType(204), ProducesResponseType(400), ProducesResponseType(403), ProducesResponseType(500), ProducesResponseType(502)]
        public async Task<IActionResult> Add([FromBody, Required] AddStockItemViewModel viewModel)
        {
            try
            {
                var retornoBLL = await _stockItemBLL.Add(viewModel);
                return StatusCode(retornoBLL.CodigoHTTP, retornoBLL);
            }
            catch (Exception erro)
            {
                return StatusCode(501, new Retorno(501, erro.Message, true));
            }
        }

        /// <summary>
        /// Remove um produto cadastrado do estoque
        /// </summary>
        /// <param name="viewModel">Classe que recebe os dados do produto a ser removido do estoque</param>
        /// <response code="200">Prpduto alterado com sucesso</response>
        /// <response code="204">Nenhum conteúdo informado com os parâmetros informados</response>
        /// <response code="400">Caso ocorra algum problema com os parâmetros informados</response>
        /// <response code="403">Chave de acesso (token) inválida, não informada ou sem permissão para acessar esta funcionalidade</response>
        /// <response code="500">Erro interno do servidor</response>
        /// <response code="502">Bad Gateway. Provavelmente um dos parâmetros estão incorretos</response>
        [HttpDelete]
        [SwaggerOperation(Tags = new[] { "Stock" })]
        [ProducesResponseType(200), ProducesResponseType(204), ProducesResponseType(400), ProducesResponseType(403), ProducesResponseType(500), ProducesResponseType(502)]
        public async Task<IActionResult> Remove([FromBody, Required] RemoveStockItemViewModel viewModel)
        {
            if (!viewModel.IDProduct.HasValue)
                return StatusCode(400, new Retorno(400, "Informe o ID do produto a ser removido", true));

            if (!viewModel.IDStore.HasValue)
                return StatusCode(400, new Retorno(400, "Informe o ID da loja associada ao produto a ser removido", true));

            var retornoBLL = await _stockItemBLL.Remove(viewModel);
            return StatusCode(retornoBLL.CodigoHTTP, retornoBLL);
        }
    }
}