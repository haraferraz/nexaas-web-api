﻿using System;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Nexaas.BLL.ViewModels;
using Nexaas.Framework.Classes;
using Nexaas.BLL.Logic;
using Nexaas.Model.UnitOfWork;
using Nexaas.BLL.Classes;
using Swashbuckle.AspNetCore.Annotations;
using Newtonsoft.Json;
using AutoMapper;

namespace Nexaas.Web.API.Controllers
{
    [ApiController, ApiVersion("1")]
    [Route("v{version:apiVersion}/store")]
    [ApiExplorerSettings(GroupName = "store")]
    public class StoreController : BaseController
    {
        private readonly StoreBLL _storeBLL;
        public StoreController(IUnitOfWork unitOfWork, IMapper mapper, IOptions<Mensagens> mensagens, IOptions<AppSettings> appSettings) :
          base(unitOfWork, mapper, mensagens, appSettings)
        {
            _storeBLL = new StoreBLL(unitOfWork, mapper);
        }

        /// <summary>
        /// Buscar a lista de lojas
        /// </summary>
        /// <response code="200">Lista retornada com sucesso</response>
        /// <response code="204">Nenhum conteúdo informado com os parâmetros informados</response>
        /// <response code="400">Caso ocorra algum problema com os parâmetros informados</response>
        /// <response code="403">Chave de acesso (token) inválida, não informada ou sem permissão para acessar esta funcionalidade</response>
        /// <response code="500">Erro interno do servidor</response>
        /// <response code="502">Bad Gateway. Provavelmente um dos parâmetros estão incorretos</response>
        [HttpGet, Route("list")]
        [SwaggerOperation(Tags = new[] { "Store" })]
        [ProducesResponseType(200), ProducesResponseType(204), ProducesResponseType(400), ProducesResponseType(403), ProducesResponseType(500), ProducesResponseType(502)]
        public async Task<IActionResult> GetStores()
        {
            try
            {
                var retornoBLL = await _storeBLL.GetList();
                return StatusCode(200, retornoBLL);
            }
            catch (Exception erro)
            {
                return StatusCode(501, new Retorno(501, erro.Message, true));
            }
        }

        /// <summary>
        /// Obtém uma loja específica de acordo com o ID informado
        /// </summary>
        /// <param name="input">Classe serializada que recebe o ID da loja a ser consultada</param>
        /// <response code="200">Loja retornada com sucesso</response>
        /// <response code="204">Nenhum conteúdo informado com os parâmetros informados</response>
        /// <response code="400">Caso ocorra algum problema com os parâmetros informados</response>
        /// <response code="403">Chave de acesso (token) inválida, não informada ou sem permissão para acessar esta funcionalidade</response>
        /// <response code="500">Erro interno do servidor</response>
        /// <response code="502">Bad Gateway. Provavelmente um dos parâmetros estão incorretos</response>
        [HttpGet]
        [SwaggerOperation(Tags = new[] { "Store" })]
        [ProducesResponseType(200), ProducesResponseType(204), ProducesResponseType(400), ProducesResponseType(403), ProducesResponseType(500), ProducesResponseType(502)]
        public async Task<IActionResult> Get([FromBody, Required] GetIDViewModel input)
        {
            try
            {
                var retornoBLL = await _storeBLL.Get(input);
                return StatusCode(200, retornoBLL);
            }
            catch (Exception erro)
            {
                return StatusCode(501, new Retorno(501, erro.Message, true));
            }
        }

        /// <summary>
        /// Insere uma loja
        /// </summary>
        /// <param name="viewModel">Classe que recebe os dados da loja a ser cadastrada</param>
        /// <response code="200">Loja cadastrada com sucesso</response>
        /// <response code="204">Nenhum conteúdo informado com os parâmetros informados</response>
        /// <response code="400">Caso ocorra algum problema com os parâmetros informados</response>
        /// <response code="403">Chave de acesso (token) inválida, não informada ou sem permissão para acessar esta funcionalidade</response>
        /// <response code="500">Erro interno do servidor</response>
        /// <response code="502">Bad Gateway. Provavelmente um dos parâmetros estão incorretos</response>
        [HttpPut]
        [SwaggerOperation(Tags = new[] { "Store" })]
        [ProducesResponseType(200), ProducesResponseType(204), ProducesResponseType(400), ProducesResponseType(403), ProducesResponseType(500), ProducesResponseType(502)]
        public async Task<IActionResult> Add([FromBody, Required] AddStoreViewModel viewModel)
        {
            try
            {
                var retornoBLL = await _storeBLL.Add(viewModel);
                return StatusCode(retornoBLL.CodigoHTTP, retornoBLL);
            }
            catch (Exception erro)
            {
                return StatusCode(501, new Retorno(501, erro.Message, true));
            }
        }

        /// <summary>
        /// Altera uma loja cadastrada
        /// </summary>
        /// <param name="viewModel">Classe que recebe os dados da loja a ser alterada</param>
        /// <response code="200">Loja alterada com sucesso</response>
        /// <response code="204">Nenhum conteúdo informado com os parâmetros informados</response>
        /// <response code="400">Caso ocorra algum problema com os parâmetros informados</response>
        /// <response code="403">Chave de acesso (token) inválida, não informada ou sem permissão para acessar esta funcionalidade</response>
        /// <response code="500">Erro interno do servidor</response>
        /// <response code="502">Bad Gateway. Provavelmente um dos parâmetros estão incorretos</response>
        [HttpPost]
        [SwaggerOperation(Tags = new[] { "Store" })]
        [ProducesResponseType(200), ProducesResponseType(204), ProducesResponseType(400), ProducesResponseType(403), ProducesResponseType(500), ProducesResponseType(502)]
        public async Task<IActionResult> Edit([FromBody, Required] EditStoreViewModel viewModel)
        {
            if (viewModel.ID == Guid.Empty)
                return StatusCode(400, new Retorno(400, "Informe o ID da loja a ser alterada", true));

            try
            {
                var retornoBLL = await _storeBLL.Edit(viewModel);
                return StatusCode(retornoBLL.CodigoHTTP, retornoBLL);
            }
            catch (Exception erro)
            {
                return StatusCode(501, new Retorno(501, erro.Message, true));
            }
        }

        /// <summary>
        /// Remove uma loja cadastrada
        /// </summary>
        /// <param name="viewModel">Classe que recebe os dados da loja a ser alterada</param>
        /// <response code="200">Loja alterada com sucesso</response>
        /// <response code="204">Nenhum conteúdo informado com os parâmetros informados</response>
        /// <response code="400">Caso ocorra algum problema com os parâmetros informados</response>
        /// <response code="403">Chave de acesso (token) inválida, não informada ou sem permissão para acessar esta funcionalidade</response>
        /// <response code="500">Erro interno do servidor</response>
        /// <response code="502">Bad Gateway. Provavelmente um dos parâmetros estão incorretos</response>
        [HttpDelete]
        [SwaggerOperation(Tags = new[] { "Store" })]
        [ProducesResponseType(200), ProducesResponseType(204), ProducesResponseType(400), ProducesResponseType(403), ProducesResponseType(500), ProducesResponseType(502)]
        public async Task<IActionResult> Remove([FromBody, Required] RemoveStoreViewModel viewModel)
        {
            if (viewModel.ID == Guid.Empty)
                return StatusCode(400, new Retorno(400, "Informe o ID da loja a ser removida", true));

            var retornoBLL = await _storeBLL.Remove(viewModel);
            return StatusCode(retornoBLL.CodigoHTTP, retornoBLL);
        }
    }
}