﻿using System;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Nexaas.BLL.ViewModels;
using Nexaas.Framework.Classes;
using Nexaas.BLL.Logic;
using Nexaas.Model.UnitOfWork;
using Nexaas.BLL.Classes;
using Swashbuckle.AspNetCore.Annotations;
using Newtonsoft.Json;
using AutoMapper;

namespace Nexaas.Web.API.Controllers
{
    [ApiController, ApiVersion("1")]
    [Route("v{version:apiVersion}/product")]
    [ApiExplorerSettings(GroupName = "product")]
    public class ProductController : BaseController
    {
        private readonly ProductBLL _productBLL;
        public ProductController(IUnitOfWork unitOfWork, IMapper mapper, IOptions<Mensagens> mensagens, IOptions<AppSettings> appSettings) :
          base(unitOfWork, mapper, mensagens, appSettings)
        {
            _productBLL = new ProductBLL(unitOfWork, mapper);
        }

        /// <summary>
        /// Buscar a lista de produtos
        /// </summary>
        /// <response code="200">Lista retornada com sucesso</response>
        /// <response code="204">Nenhum conteúdo informado com os parâmetros informados</response>
        /// <response code="400">Caso ocorra algum problema com os parâmetros informados</response>
        /// <response code="403">Chave de acesso (token) inválida, não informada ou sem permissão para acessar esta funcionalidade</response>
        /// <response code="500">Erro interno do servidor</response>
        /// <response code="502">Bad Gateway. Provavelmente um dos parâmetros estão incorretos</response>
        [HttpGet, Route("list")]
        [SwaggerOperation(Tags = new[] { "Product" })]
        [ProducesResponseType(200), ProducesResponseType(204), ProducesResponseType(400), ProducesResponseType(403), ProducesResponseType(500), ProducesResponseType(502)]
        public async Task<IActionResult> GetList()
        {
            try
            {
                var retornoBLL = await _productBLL.GetList();
                return StatusCode(200, retornoBLL);
            }
            catch (Exception erro)
            {
                return StatusCode(501, new Retorno(501, erro.Message, true));
            }
        }

        /// <summary>
        /// Obtém um produto específico de acordo com o ID informado
        /// </summary>
        /// <param name="input">Classe serializada que recebe o ID do produto a ser consultado</param>
        /// <response code="200">Produto retornado com sucesso</response>
        /// <response code="204">Nenhum conteúdo informado com os parâmetros informados</response>
        /// <response code="400">Caso ocorra algum problema com os parâmetros informados</response>
        /// <response code="403">Chave de acesso (token) inválida, não informada ou sem permissão para acessar esta funcionalidade</response>
        /// <response code="500">Erro interno do servidor</response>
        /// <response code="502">Bad Gateway. Provavelmente um dos parâmetros estão incorretos</response>
        [HttpGet]
        [SwaggerOperation(Tags = new[] { "Product" })]
        [ProducesResponseType(200), ProducesResponseType(204), ProducesResponseType(400), ProducesResponseType(403), ProducesResponseType(500), ProducesResponseType(502)]
        public async Task<IActionResult> Get([FromBody, Required] GetIDViewModel input)
        {
            try
            {
                var retornoBLL = await _productBLL.Get(input);
                return StatusCode(200, retornoBLL);
            }
            catch (Exception erro)
            {
                return StatusCode(501, new Retorno(501, erro.Message, true));
            }
        }

        /// <summary>
        /// Insere um produto
        /// </summary>
        /// <param name="viewModel">Classe que recebe os dados do produto a ser cadastrado</param>
        /// <response code="200">Produto cadastrado com sucesso</response>
        /// <response code="204">Nenhum conteúdo informado com os parâmetros informados</response>
        /// <response code="400">Caso ocorra algum problema com os parâmetros informados</response>
        /// <response code="403">Chave de acesso (token) inválida, não informada ou sem permissão para acessar esta funcionalidade</response>
        /// <response code="500">Erro interno do servidor</response>
        /// <response code="502">Bad Gateway. Provavelmente um dos parâmetros estão incorretos</response>
        [HttpPut]
        [SwaggerOperation(Tags = new[] { "Product" })]
        [ProducesResponseType(200), ProducesResponseType(204), ProducesResponseType(400), ProducesResponseType(403), ProducesResponseType(500), ProducesResponseType(502)]
        public async Task<IActionResult> Add([FromBody, Required] AddProductViewModel viewModel)
        {
            try
            {
                var retornoBLL = await _productBLL.Add(viewModel);
                return StatusCode(retornoBLL.CodigoHTTP, retornoBLL);
            }
            catch (Exception erro)
            {
                return StatusCode(501, new Retorno(501, erro.Message, true));
            }
        }

        /// <summary>
        /// Altera um produto cadastrado
        /// </summary>
        /// <param name="viewModel">Classe que recebe os dados do produto a ser alterado</param>
        /// <response code="200">Produto alterado com sucesso</response>
        /// <response code="204">Nenhum conteúdo informado com os parâmetros informados</response>
        /// <response code="400">Caso ocorra algum problema com os parâmetros informados</response>
        /// <response code="403">Chave de acesso (token) inválida, não informada ou sem permissão para acessar esta funcionalidade</response>
        /// <response code="500">Erro interno do servidor</response>
        /// <response code="502">Bad Gateway. Provavelmente um dos parâmetros estão incorretos</response>
        [HttpPost]
        [SwaggerOperation(Tags = new[] { "Product" })]
        [ProducesResponseType(200), ProducesResponseType(204), ProducesResponseType(400), ProducesResponseType(403), ProducesResponseType(500), ProducesResponseType(502)]
        public async Task<IActionResult> Edit([FromBody, Required] EditProductViewModel viewModel)
        {
            if (viewModel.ID == Guid.Empty)
                return StatusCode(400, new Retorno(400, "Informe o ID do produto a ser alterado", true));

            try
            {
                var retornoBLL = await _productBLL.Edit(viewModel);
                return StatusCode(retornoBLL.CodigoHTTP, retornoBLL);
            }
            catch (Exception erro)
            {
                return StatusCode(501, new Retorno(501, erro.Message, true));
            }
        }

        /// <summary>
        /// Remove um produto cadastrado
        /// </summary>
        /// <param name="viewModel">Classe que recebe os dados do produto a ser alterado</param>
        /// <response code="200">Prpduto alterado com sucesso</response>
        /// <response code="204">Nenhum conteúdo informado com os parâmetros informados</response>
        /// <response code="400">Caso ocorra algum problema com os parâmetros informados</response>
        /// <response code="403">Chave de acesso (token) inválida, não informada ou sem permissão para acessar esta funcionalidade</response>
        /// <response code="500">Erro interno do servidor</response>
        /// <response code="502">Bad Gateway. Provavelmente um dos parâmetros estão incorretos</response>
        [HttpDelete]
        [SwaggerOperation(Tags = new[] { "Product" })]
        [ProducesResponseType(200), ProducesResponseType(204), ProducesResponseType(400), ProducesResponseType(403), ProducesResponseType(500), ProducesResponseType(502)]
        public async Task<IActionResult> Remove([FromBody, Required] RemoveProductViewModel viewModel)
        {
            if (viewModel.ID == Guid.Empty)
                return StatusCode(400, new Retorno(400, "Informe o ID do produto a ser removido", true));

            var retornoBLL = await _productBLL.Remove(viewModel);
            return StatusCode(retornoBLL.CodigoHTTP, retornoBLL);
        }
    }
}