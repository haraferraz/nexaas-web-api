using System;
using System.Globalization;
using System.Collections.Generic;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Rewrite;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Nexaas.Model;
using Nexaas.Web.API.Filters;
using Nexaas.Web.API.Extensions;
using Nexaas.Web.API.Authentication;
using Nexaas.Web.API.Providers;
using Nexaas.BLL.Classes;
using Nexaas.BLL.Mappers;
using Nexaas.Model.UnitOfWork;
using AutoMapper;

namespace Nexaas.Web.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<Context>(
              options => {
                  options.UseSqlServer(Configuration.GetConnectionString("Banco"));
              },
              ServiceLifetime.Transient);

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new EntidadeParaViewModelMapping());
                mc.AddProfile(new ViewModelParaEntidadeMapping());
            });

            services.AddResponseCaching();
            services.AddSwaggerDocumentation();
            services.AddResponseCompression(options =>
            {
                options.Providers.Add<BrotliCompressionProvider>();
                options.EnableForHttps = true;
            });

            services.AddTransient<IUnitOfWork, UnitOfWork>();
            services.AddSingleton(mappingConfig.CreateMapper());
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            var appSettingsSection = Configuration.GetSection("AppSettings");
            services.Configure<Mensagens>(Configuration.GetSection("Mensagens"));
            services.Configure<AppSettings>(appSettingsSection);

            var appSettings = appSettingsSection.Get<AppSettings>();

            services.AddApiVersioning(options =>
            {
                options.AssumeDefaultVersionWhenUnspecified = true;
            });
            services.AddMvc(options =>
            {
                options.Filters.Add<VerificarPermissaoActionFilter>();
            });
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
              .AddJwtBearer(options =>
              {
                  options.TokenValidationParameters =
              new TokenValidationParameters
                  {
                      ValidateIssuer = true,
                      ValidateAudience = true,
                      ValidateLifetime = true,
                      ValidateIssuerSigningKey = true,
                      ValidIssuer = appSettings.TokenIssuer,
                      ValidAudience = appSettings.TokenAudience,
                      IssuerSigningKey = JwtSecurityKey.Create(appSettings.ChavePrivadaApi)
                  };
              });

            services.Configure<MvcOptions>(options => {
                options.Filters.Add(new RequireHttpsAttribute());
                options.EnableEndpointRouting = false;
            });

            services.AddControllers().AddNewtonsoftJson();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            app.Use(async (ctx, next) =>
            {
                ctx.Request.GetTypedHeaders().CacheControl = new CacheControlHeaderValue()
                {
                    Public = true,
                    MaxAge = TimeSpan.FromHours(1)
                };
                await next();
            });
            app.UseResponseCompression();

            var builder = new ConfigurationBuilder()
              .SetBasePath(env.ContentRootPath)
              .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
              .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
              .AddEnvironmentVariables();

            builder.Build();

            var optionsHttps = new RewriteOptions()
              .AddRedirectToHttps(StatusCodes.Status301MovedPermanently);

            if (env.EnvironmentName == "Development")
            {
                app.UseDeveloperExceptionPage();
                app.UseSwaggerDocumentation();
            }
            else
            {
                app.UseHsts();
            }

            app.UseRewriter(optionsHttps);
            app.UseAuthentication();
            app.UseResponseCaching();
            app.UseRequestLocalization(new RequestLocalizationOptions
            {
                SupportedCultures = new List<CultureInfo> { new CultureInfo("pt-BR") },
                SupportedUICultures = new List<CultureInfo> { new CultureInfo("pt-BR") },
                DefaultRequestCulture = new RequestCulture("pt-BR")
            });

            app.UseMvc();

            app.UseExceptionHandler(
               options =>
               {
                   options.Run(
             async contexto =>
                 {
                     contexto.Response.StatusCode = StatusCodes.Status500InternalServerError;
                     contexto.Response.ContentType = "json";
                     var ex = contexto.Features.Get<IExceptionHandlerFeature>();

                     if (ex != null)
                     {
                         var err = $"Ocorreu um erro ao processar sua solicitação: {ex.Error.Message}";
                         await contexto.Response.WriteAsync(err).ConfigureAwait(false);
                     }
                 });
               });
        }
    }
}