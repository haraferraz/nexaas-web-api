﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Builder;
using Microsoft.Extensions.DependencyInjection;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace Nexaas.Web.API.Extensions
{
    public static class SwaggerServiceExtensions
    {
        public static IServiceCollection AddSwaggerDocumentation(this IServiceCollection services)
        {
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("autenticacao", new OpenApiInfo { Title = "Autenticação", Version = "v1" });
                c.SwaggerDoc("store", new OpenApiInfo { Title = "Store", Version = "v1" });
                c.SwaggerDoc("product", new OpenApiInfo { Title = "Product", Version = "v1" });
                c.SwaggerDoc("stock", new OpenApiInfo { Title = "Stock", Version = "v1" });

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
                {
                    Description = "Primeiramente, utilize o método Token da API para gerar seu bearer token necessário para realizar as outras requisições. Para gerar o token é necessário informar sua chave de acesso. O token gerado é válido por 8 horas.",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });

                c.AddSecurityRequirement(new OpenApiSecurityRequirement {
         {
           new OpenApiSecurityScheme
           {
             Reference = new OpenApiReference
             {
               Type = ReferenceType.SecurityScheme,
               Id = "Bearer"
             }
            },
            new string[] { }
          }
              });
                c.EnableAnnotations();

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            return services;
        }

        public static IApplicationBuilder UseSwaggerDocumentation(this IApplicationBuilder app)
        {
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.RoutePrefix = "docs";
                c.DocExpansion(DocExpansion.List);

                c.SwaggerEndpoint("../swagger/autenticacao/swagger.json", "Autenticação");
                c.SwaggerEndpoint("../swagger/store/swagger.json", "Stores");
                c.SwaggerEndpoint("../swagger/product/swagger.json", "Product");
                c.SwaggerEndpoint("../swagger/stock/swagger.json", "Stock Items");
            });

            return app;
        }
    }
}