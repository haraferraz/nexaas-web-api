﻿using System;
using System.IdentityModel.Tokens.Jwt;

namespace Nexaas.Web.API.Authentication
{
    public sealed class JwtToken
    {
        private JwtSecurityToken token;

        internal JwtToken(JwtSecurityToken token)
        {
            this.token = token;
        }

        public string Token => new JwtSecurityTokenHandler().WriteToken(this.token);
        public DateTime ValidoAte => token.ValidTo;
        public bool Erro => false;
    }
}