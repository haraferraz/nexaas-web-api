﻿using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;
using Nexaas.Framework.Enums;
using CryptSharp;

namespace Nexaas.Framework.Seguranca
{
    public static class Criptografia
    {
        public static string GerarSenhaRandom(int tamanho)
        {
            const string valido = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890*&#@";
            var res = new StringBuilder();
            var rnd = new Random();

            while (0 < tamanho--)
                res.Append(valido[rnd.Next(valido.Length)]);

            return res.ToString();
        }
        public static string Criptografar(string texto, TipoCriptografia tipo, string senha = "")
        {
            try
            {
                switch (tipo)
                {
                    case TipoCriptografia.MD5: return CriptografarMD5(texto);
                    case TipoCriptografia.Base64: return CriptografarBase64(texto);
                    case TipoCriptografia.SHA512: return Crypter.Sha512.Crypt(texto);
                    case TipoCriptografia.Blowfish: return Crypter.Blowfish.Crypt(texto);
                    case TipoCriptografia.BCrypt: return BCrypt.Net.BCrypt.HashPassword(texto, BCrypt.Net.BCrypt.GenerateSalt(12));
                    case TipoCriptografia.AES128: return CriptografarAES(texto, senha, tipo);
                    case TipoCriptografia.AES256: return CriptografarAES(texto, senha, tipo);
                    default: return Crypter.Sha256.Crypt(texto);
                }
            }
            catch (Exception erro) { throw erro; }
        }
        public static string Descriptografar(string texto, TipoCriptografia tipo)
        {
            try
            {
                switch (tipo)
                {
                    case TipoCriptografia.Base64: return DescriptografarBase64(texto);
                    default: return string.Empty;
                }
            }
            catch (Exception erro) { throw erro; }
        }
        public static string Descriptografar(byte[] texto, TipoCriptografia tipo, string senha = "")
        {
            try
            {
                switch (tipo)
                {
                    case TipoCriptografia.AES128: return DescriptografarAES(texto, senha, tipo);
                    case TipoCriptografia.AES256: return DescriptografarAES(texto, senha, tipo);
                    default: return string.Empty;
                }
            }
            catch (Exception erro) { throw erro; }
        }
        public static bool VerificarValoresCriptografados(string texto, string textoCriptografado, TipoCriptografia tipo)
        {
            try
            {
                switch (tipo)
                {
                    case TipoCriptografia.SHA256:
                    case TipoCriptografia.SHA512:
                    case TipoCriptografia.Blowfish:
                    default: return BCrypt.Net.BCrypt.Verify(texto, textoCriptografado);
                }
            }
            catch (Exception erro) { throw erro; }
        }

        private static string CriptografarMD5(string texto)
        {
            try
            {
                var md5 = new MD5CryptoServiceProvider();

                md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(texto));

                byte[] result = md5.Hash;

                var strBuilder = new StringBuilder();

                for (int i = 0; i < result.Length; i++)
                    strBuilder.Append(result[i].ToString("x2"));

                return strBuilder.ToString();
            }
            catch (Exception erro) { throw erro; }
        }
        private static string CriptografarBase64(string texto)
        {
            var bytes = Encoding.UTF8.GetBytes(texto);
            return Convert.ToBase64String(bytes);
        }
        private static string DescriptografarBase64(string textoCriptografado)
        {
            var bytes = Convert.FromBase64String(textoCriptografado);
            return Encoding.UTF8.GetString(bytes);
        }
        private static string CriptografarAES(string texto, string key, TipoCriptografia tipoCriptografia)
        {
            byte[] encrypted;

            using (var aes = new AesManaged())
            {
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;
                var encryptor = aes.CreateEncryptor(Encoding.UTF8.GetBytes(key), tipoCriptografia == TipoCriptografia.AES128 ? new byte[16] : new byte[32]);

                using (var ms = new MemoryStream())
                {
                    using (var cs = new CryptoStream(ms, encryptor, CryptoStreamMode.Write))
                    {
                        using (var sw = new StreamWriter(cs))
                            sw.Write(texto);

                        encrypted = ms.ToArray();
                    }
                }
            }
            return Convert.ToBase64String(encrypted);
        }
        private static string DescriptografarAES(byte[] texto, string key, TipoCriptografia tipoCriptografia)
        {
            string plainText = null;

            using (var aes = new AesManaged())
            {
                aes.Mode = CipherMode.CBC;
                aes.Padding = PaddingMode.PKCS7;
                var decryptor = aes.CreateDecryptor(Encoding.UTF8.GetBytes(key), tipoCriptografia == TipoCriptografia.AES128 ? new byte[16] : new byte[32]);

                using (var ms = new MemoryStream(texto))
                {
                    using (var cs = new CryptoStream(ms, decryptor, CryptoStreamMode.Read))
                    {
                        using (var reader = new StreamReader(cs))
                            plainText = reader.ReadToEnd();
                    }
                }
            }
            return plainText;
        }
    }
}