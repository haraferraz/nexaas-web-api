﻿using System;

namespace Nexaas.Framework.Classes
{
    public class RetornoPermissao
    {
        public Guid IDSistemaChave { get; set; }
        public bool Permitido { get; set; }
    }
}