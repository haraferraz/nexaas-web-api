﻿namespace Nexaas.Framework.Classes
{
  public class ListaInt
  {
    public int Valor { get; set; }
    public string Texto { get; set; }
    public bool Selecionado { get; set; }
  }
}
