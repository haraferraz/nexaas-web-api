﻿namespace Nexaas.Framework.Classes
{
  public class ObjetoGenerico
  {
    public string Nome { get; set; }
    public object Valor { get; set; }
  }
}