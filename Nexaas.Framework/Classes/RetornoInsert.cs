﻿using System;

namespace Nexaas.Framework.Classes
{
    public class RetornoInsert : Retorno
    {
        public Guid ID { get; set; }

        public RetornoInsert() { }
        public RetornoInsert(int codigoHTTP, string mensagem, bool erro, Guid id) : base(codigoHTTP, mensagem, erro)
        {
            ID = id;
        }
    }
}