﻿namespace Nexaas.Framework.Classes
{
    public class RetornoJson
    {
        public bool Erro { get; set; }
        public string Mensagem { get; set; }
        public string UrlRetorno { get; set; }

        public RetornoJson(bool erro, string mensagem, string urlRetorno)
        {
            Erro = erro;
            Mensagem = mensagem;
            UrlRetorno = urlRetorno;
        }
    }
}
