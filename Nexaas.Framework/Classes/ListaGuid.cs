﻿using System;

namespace Nexaas.Framework.Classes
{
  public class ListaGuid
  {
    public Guid Valor { get; set; }
    public string Texto { get; set; }
    public bool Selecionado { get; set; }
  }
}
