﻿namespace Nexaas.Framework.Classes
{
  public class ListaBool
  {
    public bool Valor { get; set; }
    public string Texto { get; set; }
    public bool Selecionado { get; set; }
  }
}
