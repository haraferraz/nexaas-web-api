﻿using System;

namespace Nexaas.Framework.Classes
{
  [Serializable]
  public class ObjetoString
  {
    public string Nome { get; set; }
    public string Valor { get; set; }
  }
}