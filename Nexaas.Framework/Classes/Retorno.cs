﻿namespace Nexaas.Framework.Classes
{
    public class Retorno
    {
        public int CodigoHTTP { get; set; }
        public string Mensagem { get; set; }
        public bool Erro { get; set; }

        public Retorno() { }
        public Retorno(int codigoHTTP, string mensagem, bool erro)
        {
            CodigoHTTP = codigoHTTP;
            Mensagem = mensagem;
            Erro = erro;
        }
    }
}