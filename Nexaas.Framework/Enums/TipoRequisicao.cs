﻿namespace Nexaas.Framework.Enums
{
    public enum TipoRequisicao
    {
        GET,
        POST,
        PUT,
        DELETE
    }
}