﻿namespace Nexaas.Framework.Enums
{
    public enum TipoCriptografia
    {
        MD5,
        Base64,
        SHA256,
        SHA512,
        BCrypt,
        Blowfish,
        AES128,
        AES256
    }
}